<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Sensors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sensors', function (Blueprint $table) {
            $table->charset     =   'utf8mb4';
            $table->collation   =   'utf8mb4_unicode_ci';

            $table->id()->autoIncrement();
            $table->timestamps();
            $table->string('name', 255)->nullable()->default(NULL);
            $table->string('location', 255)->nullable()->default(NULL);
            $table->json('showing_values', 255)->default(json_encode(['temp']));
            $table->enum('visibility', ['off', 'on'])->default('on');
            $table->enum('temp_alert_activity', ['off', 'on'])->default('off');
            $table->decimal('temp_min', 10,1)->nullable()->default(NULL);
            $table->decimal('temp_max', 10,1)->nullable()->default(NULL);
            $table->enum('humi_alert_activity', ['off', 'on'])->default('off');
            $table->decimal('humi_min', 10,1)->nullable()->default(NULL);
            $table->decimal('humi_max', 10,1)->nullable()->default(NULL);
            $table->enum('press_alert_activity', ['off', 'on'])->default('off');
            $table->decimal('press_min', 10,1)->nullable()->default(NULL);
            $table->decimal('press_max', 10,1)->nullable()->default(NULL);
            $table->integer('station_id')->length(11);
            $table->integer('sensor_id')->length(11);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('sensors');
    }
}
