<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SensorsConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sensors_config', function (Blueprint $table) {
            $table->charset     =   'utf8mb4';
            $table->collation   =   'utf8mb4_unicode_ci';

            $table->id()->autoIncrement();
            $table->timestamps();
            $table->integer('number')->length(11);
            $table->string('location', 255)->nullable()->default(NULL);
            $table->decimal('temp_diff', 10,1)->default(0);
            $table->decimal('humi_diff', 10,1)->default(0);
            $table->decimal('press_diff', 10,1)->default(0);
            $table->integer('station_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('autoraports');
    }
}
