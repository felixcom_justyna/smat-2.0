<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Settings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->charset     =   'utf8mb4';
            $table->collation   =   'utf8mb4_unicode_ci';

            $table->id()->autoIncrement();
            $table->timestamps();
            $table->string('main_header', 255)->default('SYSTEM POMIARU PARAMETRÓW ŚRODOWISKOWYCH');
            $table->string('second_header', 255)->default('DLA OBIEKTU - ');
            $table->string('company_name', 255)->nullable()->default(NULL);
            $table->string('company_logo', 255)->nullable()->default(NULL);
            $table->enum('csv_status', ['off', 'on'])->default('off');
            $table->enum('linechart_status', ['off', 'on'])->default('on');
            $table->enum('piechart_status', ['off', 'on'])->default('on');
            $table->string('chart_temp_color', 255)->default('rgb(200, 0, 0)');
            $table->string('chart_press_color', 255)->default('rgb(48, 188, 237)');
            $table->string('chart_humi_color', 255)->default('rgb(40, 85, 143)');
        });
        // insert default data
        DB::table('settings')->insert(array('id' => 1));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('settings');
    }
}
