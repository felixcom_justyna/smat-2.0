<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChartsFilters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charts_filters', function (Blueprint $table) {
            $table->charset     =   'utf8mb4';
            $table->collation   =   'utf8mb4_unicode_ci';

            $table->id()->autoIncrement();
            $table->timestamps();
            $table->enum('type', ['days', 'interval']);
            $table->integer('number')->length(11);
            $table->string('name', 255);
            $table->enum('status', ['off', 'on'])->default('on');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('charts_filters');
    }
}
