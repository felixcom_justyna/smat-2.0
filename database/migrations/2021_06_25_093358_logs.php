<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Logs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('logs', function (Blueprint $table) {
            $table->charset     =   'utf8mb4';
            $table->collation   =   'utf8mb4_unicode_ci';

            $table->id()->autoIncrement();
            $table->timestamps();
            $table->datetime('date')->useCurrent = true;
            $table->enum('type', ['info', 'error', 'notice'])->default('info');
            $table->string('user', 255);
            $table->string('content', 500);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('logs');
    }
}
