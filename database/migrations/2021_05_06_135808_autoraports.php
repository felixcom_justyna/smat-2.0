<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Autoraports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autoraports', function (Blueprint $table) {
            $table->charset     =   'utf8mb4';
            $table->collation   =   'utf8mb4_unicode_ci';

            $table->id()->autoIncrement();
            $table->timestamps();
            $table->json('emails', 255);
            $table->json('sending_days')->default(json_encode(['all']));
            $table->time('sending_time')->default('00:00:00');
            $table->time('start_time')->default('00:00:00');
            $table->integer('frequency')->length(11)->default('1');
            $table->integer('interval')->length(11);
            $table->json('stations')->default(json_encode(['all']));
            $table->datetime('status')->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('autoraports');
    }
}
