<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TmpTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tmp_time', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->timestamps();
            $table->datetime('date')->unique()->useCurrent = true;
            $table->decimal('temp', 10,1)->nullable()->default(NULL);
            $table->decimal('press', 10,1)->nullable()->default(NULL);
            $table->decimal('humi', 10,1)->nullable()->default(NULL);
        });
    }

    public function down() {
        Schema::dropIfExists('tmp_time');
    }
}
