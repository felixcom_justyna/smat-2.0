<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->charset     =   'utf8mb4';
            $table->collation   =   'utf8mb4_unicode_ci';

            $table->id()->autoIncrement();
            $table->timestamps();
            $table->string('username', 255)->unique();
            $table->string('password', 255);
            $table->string('name', 255)->nullable()->default(NULL);
            $table->string('surname', 255)->nullable()->default(NULL);
            $table->string('phone_number', 255)->nullable()->default(NULL);
            $table->enum('user_type', ['user', 'admin', 'dev'])->default('user');
            $table->json('visible_stations')->nullable()->default(json_encode(['all']));
            $table->enum('alert_raport', ['off', 'on'])->default('off');
            $table->enum('auto_raport', ['off', 'on'])->default('off');
            $table->dateTime('logged')->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
