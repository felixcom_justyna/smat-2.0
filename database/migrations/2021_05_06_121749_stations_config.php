<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StationsConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stations_config', function (Blueprint $table) {
            $table->charset     =   'utf8mb4';
            $table->collation   =   'utf8mb4_unicode_ci';

            $table->id()->autoIncrement();
            $table->timestamps();
            $table->string('serial_number', 255)->unique();
            $table->string('location', 255);
            $table->date('calibration_date')->nullable()->default(NULL);
            $table->integer('calibration_days')->length(11)->nullable()->default(NULL); //1 year - 365, 2 years - 730, 5years - 1825
            $table->date('calibration_end_date')->nullable()->default(NULL);
            $table->date('calibration_info_sended')->nullable()->default(NULL);
            $table->datetime('status')->nullable()->default(NULL);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('stations_config');
    }
}
