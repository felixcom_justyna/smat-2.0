<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DetectedAlerts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detected_alerts', function (Blueprint $table) {
            $table->charset     =   'utf8mb4';
            $table->collation   =   'utf8mb4_unicode_ci';

            $table->id()->autoIncrement();
            $table->timestamps();
            $table->string('serial_number', 255);
            $table->integer('sensor_id')->length(11);
            $table->string('value_type', 255);
            $table->enum('alert_type', ['global', 'individual']);
            $table->decimal('measured_value', 10,1);
            $table->datetime('date')->useCurrent = true;
            $table->datetime('end_date')->nullable()->default(NULL);
            $table->datetime('status')->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('detected_alerts');
    }
}
