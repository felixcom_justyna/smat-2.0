<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FiltersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('charts_filters')->insert(array(
            [
                'type'      =>  'days',
                'number'    =>  1,
                'name'      =>  '24h'
            ],
            [
                'type'      =>  'days',
                'number'    =>  7,
                'name'      =>  'Tydzień'
            ],
            [
                'type'      =>  'days',
                'number'    =>  31,
                'name'      =>  'Miesiąc'
            ],
            [
                'type'      =>  'days',
                'number'    =>  90,
                'name'      =>  'Kwartał'
            ],
            [
                'type'      =>  'interval',
                'number'    =>  300,
                'name'      =>  '5min'
            ],
            [
                'type'      =>  'interval',
                'number'    =>  900,
                'name'      =>  '15min'
            ],
            [
                'type'      =>  'interval',
                'number'    =>  1800,
                'name'      =>  '30min'
            ],
            [
                'type'      =>  'interval',
                'number'    =>  3600,
                'name'      =>  '1h'
            ],
            
            [
                'type'      =>  'interval',
                'number'    =>  21600,
                'name'      =>  '6h'
            ]
        ));
    }
}
