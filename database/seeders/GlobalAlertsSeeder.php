<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GlobalAlertsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('alerts_limits')->insert(array(
            ['value' => 'temp'],
            ['value' => 'press'],
            ['value' => 'humi'],
        ));
    }
}
