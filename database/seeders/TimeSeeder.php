<?php

namespace Database\Seeders;

use App\Models\Time;
use Illuminate\Database\Seeder;

class TimeSeeder extends Seeder
{
    private $dateTimes = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = now();
        for ($i = 1; $i <= 1440; ++$i) {
            $newTime = $now->addMinutes(-1)->format('Y-m-d H:i');
            $dateTimes[] = [
                'date' => $newTime
            ];
        }
        foreach ($dateTimes as $record) {
            Time::create($record);
        }
    }
}
