<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            // [
            //     'username'      =>  'biuro@felixcom.pl',
            //     'password'      =>  Hash::make('hasłoszefa'),
            //     'name'          =>  'Krzysztof',
            //     'surname'       =>  'Feliksik',
            //     'phone_number'  =>  '',
            //     'user_type'     =>  'dev',
            // ],
            [
                'username' => 'justyna@felixcom.pl',
                'password' => Hash::make('testowe20'),
                'name' => 'Justyna',
                'surname' => 'Bilaszewska',
                'phone_number' => '+48664451476',
                'user_type' => 'dev',
            ]
        ));
    }
}
