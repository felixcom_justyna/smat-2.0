<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Error extends Mailable
{
    use Queueable, SerializesModels;

    public $company, $errors;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($company, $errors) {
        $this->company  = $company;
        $this->errors   = $errors;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->subject('Odnotowano błąd')->markdown('emails.error');
    }
}
