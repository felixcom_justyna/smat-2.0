<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GlobalAlert extends Mailable
{
    use Queueable, SerializesModels;

    public $company, $alerts, $limits, $vals;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($company, $alerts, $limits) {
        $this->company  = $company;
        $this->alerts   = $alerts;
        $this->limits   = $limits;

        foreach($this->limits as $limit) {
            $this->vals[$limit->value] = [
                'min'   =>  $limit->min,
                'max'   =>  $limit->max
            ];
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->subject('Odnotowano przekroczenie progów globalnych')->markdown('emails.global');
    }
}
