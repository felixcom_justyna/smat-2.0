<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Calibration extends Mailable
{
    use Queueable, SerializesModels;

    public $company, $serial;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($company, $serial) {
        $this->company = $company;
        $this->serial = $serial;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->subject('Zgłoszenie ponownego wzorcowania')
                    ->markdown('emails.calibration');
    }
}
