<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class IndividualAlert extends Mailable
{
    use Queueable, SerializesModels;

    public $company, $alerts, $stations, $flag;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($company, $alerts, $stations, $flag) {
        $this->flag     =   $flag;
        $this->company  =   $company;
        $this->alerts   =   $alerts;
        $this->stations =   $stations;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->subject('Odnotowano przekroczenie progów szczególnych')->markdown('emails.individual');
    }
}
