<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Raport extends Mailable
{
    use Queueable, SerializesModels;

    public $filesPath;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($filesPath) {
        $this->filesPath  = $filesPath;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        foreach ($this->filesPath as $key => $path) {
            $this->attach($path);
        }
        return $this->subject('Automatyczny raport systemu SMAT')
                    ->markdown('emails.autoraport');
    }
}
