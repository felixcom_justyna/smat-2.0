<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Offline extends Mailable
{
    use Queueable, SerializesModels;
    public $company, $serials;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($company, $serials) {
        $this->company  = $company;
        $this->serials  = $serials;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->subject('Odnotowano OFFLINE stacji')->markdown('emails.offline');
    }
}
