<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;

class StatisticsExport implements FromArray
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public $data;

    public function __construct($statistics)
    {
        $this->data = $statistics;
    }

    public function array(): array
    {
        return $this->data;
    }
}
