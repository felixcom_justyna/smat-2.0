<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Support\Facades\Mail;
use App\Mail\Offline;
use App\Mail\Error;
use App\Mail\GlobalAlert;
use App\Mail\IndividualAlert;

class MailingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $type, $email, $company, $data, $stations, $flag;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($type, $email, $company, $data, $stations = false, $flag = false) 
    {
        $this->type = $type;
        $this->email = $email;
        $this->company = $company;
        $this->data = $data;
        $this->stations = $stations;
        $this->flag = $flag;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {   
        switch ($this->type) {
            case 'offline': {
                Mail::to($this->email)->send(new Offline($this->company, $this->data));
            } break;
            case 'error': {
                Mail::to($this->email)->send(new Error($this->company, $this->data));
            } break;
            case 'alerts-global': {
                Mail::to($this->email)->send(new GlobalAlert($this->company, $this->data, $this->stations, $this->flag));
            } break;
            case 'indv-alerts-toAll': {
                Mail::to($this->email)->send(new IndividualAlert($this->company, $this->data, $this->stations, $this->flag));
            } break;
            case 'indv-alerts-toUser': {
                Mail::to($this->email)->send(new IndividualAlert($this->company, $this->data, $this->stations, $this->flag));
            } break;
        }
    }
}
