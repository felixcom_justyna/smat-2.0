<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

class InsertTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'time:everyminute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert every minute to db table "tmp_time"';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() {
        $connections = Config::get('database.connections');
        if (count($connections) > 0) {
            foreach ($connections as $key => $val) {
                DB::connection($key)->table('tmp_time')->insert(['date' => date("Y-m-d H:i:s")]);
            }
        }
    }
}
