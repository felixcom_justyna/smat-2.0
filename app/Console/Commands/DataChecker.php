<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Mail;
use App\Jobs\MailingJob;
use App\Mail\IndividualAlert;

use Config;

use App\Models\Smat;
use App\Models\User;
use App\Http\Controllers\SmatController;
use App\Http\Controllers\UserController;

class DataChecker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:checker';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check errors and limits to send info mail';

    private $now, $connections, $devs, $stations, $newStations, $company, $offlines, $errors, $alerts;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->now          = date('Y-m-d H:i');
        $this->connections  = Config::get('database.connections');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() {
        
        if (count($this->connections) > 0) {
            foreach ($this->connections as $dbName => $dbConfig) {
                if (str_contains($dbName, config('app.name'))) {
                    try {
                        // set database & queue
                        config(['database.default' => $dbName]);
                        // get data
                        $devs            =   new UserController;
                        $this->devs      =   $devs->getDevs();
                        $this->company   =   $this->prepareData('settings');
                        $this->errors    =   $this->prepareData('detected_errors');
                        $this->alerts = [
                            'global'        => $this->prepareData('detected_alerts', 'global'),
                            'individual'    => $this->prepareData('detected_alerts', 'individual')
                        ];
                        // check last record
                        $this->checkOfflines();
                        // check errors
                        $this->checkErrors();
                        // check limits
                        $this->checkLimits();
                    } catch(\Exception $e){
                        $this->info($e);
                        die();
                    }
                }
            }
        }
    }
    /**
     * Get & prepare needed data from database 
     *
     * @param $table
     * @param $flag
     * @return array
     */
    private function prepareData($table, $flag = false) {
        $removeFresh = function($data) {
            foreach ($data as $row => $arr) {
                if ($arr->status !== NULL) {
                    $diff = (strtotime($this->now) - strtotime($arr->status)) / 3600;
                    if ((float)$diff < 1) {
                        unset($data[$row]);
                    }
                } 
            }
            return $data;
        };
        switch($table) {
            case 'settings': {
                $data = DB::table($table)
                            ->select('company_name')
                            ->first();
            } break;
            case 'detected_errors': {
                $data = DB::table($table)
                            ->select("id", "serial_number", "sensor_id as number", "value_type", "error_type", "date", "end_date", "status")
                            ->whereNull('end_date')
                            ->get();
                if (count($data) > 0) {
                    // remove rows with fresh status
                    $data = $removeFresh($data);
                }
            } break;
            case 'detected_alerts': {
                $data = DB::table($table)
                            ->select("id", "serial_number", "sensor_id as number", "value_type", "alert_type", "measured_value","date", "end_date", "status")
                            ->where('alert_type', '=', $flag)
                            ->whereNull('end_date')
                            ->get();
                if (count($data) > 0) {
                    // remove rows with fresh status
                    $data = $removeFresh($data);
                }
            } break;
            case 'global-limits': {
                $data = DB::table('alerts_limits')
                            ->select('value', 'min', 'max')
                            ->where('status', '=', 'on')
                            ->get();
            }
        }
        return $data;
    }
    /**
     * Check if station is offline
     *
     */
    private function checkOfflines() {
        // get all db tables
        $tables = array_map(function($n) {
            foreach ($n as $key => $val) {
                return $val;
            }
        }, DB::select('SHOW TABLES'));

        foreach ($tables as $key => $tableName) {
            // find tables with station data
            if (strpos($tableName, strtolower(config('app.name'))) !== false) {
                // get the freshest record
                $lastRecord = DB::table($tableName)
                                ->select('date')
                                ->orderBy('date', 'DESC')
                                ->first();
                $check = false;
                if (isset($lastRecord->date) && $lastRecord->date !== NULL) {
                    // check diff, 10minutes
                    $diff = (int)((strtotime($this->now) - strtotime($lastRecord->date)) / 60);
                    if (is_numeric($diff) && $diff >= 10) {
                        //check station offline status
                        $smatBox = new SmatController;
                        $record = $smatBox->getSmatBox($tableName);
                        $datetime = $record['smat']->status;
                        if ($datetime !== NULL) {
                            //check diff, 1h
                            $diff = (int)((strtotime($this->now) - strtotime($datetime)) / 3600);
                            $check = ($diff >= 1) ? true : false;
                        }
                        // check if offline
                        if ($tableName !== NULL) {
                            if ($datetime === NULL || ($check !== NULL && $check)) {
                                $this->offlines[] = $tableName;
                            }
                        }
                    }
                }
            }
        }
        if ($this->offlines !== NULL && count($this->offlines) > 0) {
            //send emails
            $this->sendTo($this->devs, 'offline');
            // update status for stations
            $this->updateStatus($this->offlines, false, 'offlines');
            $this->offlines = NULL;
        }
    }
    /**
     * Check if station has error
     *
     */
    private function checkErrors() {
        if ($this->errors !== NULL && count($this->errors) > 0) {
            $this->sendTo($this->devs, 'error');
            // update errors status
            $this->updateStatus($this->errors, 'detected_errors');
            $this->errors = NULL;
        }
    }
    /**
     * Check if station's data are off scale
     *
     */
    private function checkLimits() {
        // get all users with "on" status to send alerts
        $users = DB::table('users')
                    ->select('username', 'visible_stations')
                    ->where('alert_raport', '=', 'on')
                    ->get();
        // prepare and send global limits
        $this->prepareGlobalLimits($users);
        // get sensors for detected individual alerts
        if ($this->alerts['individual'] !== NULL && count($this->alerts['individual']) > 0) {
            $this->stations = NULL;
            foreach ($this->alerts['individual'] as $alert) {
                $sensors = DB::table('sensors')
                            ->join('sensors_config', 'sensors_config.id', '=', 'sensors.sensor_id')
                            ->join('stations', 'stations.id', '=', 'sensors.station_id')
                            ->join('stations_config', 'sensors_config.station_id', '=', 'stations_config.id')
                            ->select(
                                'sensors.name as sensorName',
                                'sensors.location as sensorLocation',
                                'sensors_config.number as sensorNumber',
                                'sensors.'.$alert->value_type.'_min', 
                                'sensors.'.$alert->value_type.'_max', 
                                'stations.id as stationId',
                                'stations_config.serial_number as serialNumber',
                                'stations.name as stationName',
                                'stations.location as stationLocation'
                            )
                            ->where([
                                ['stations_config.serial_number', '=', $alert->serial_number],
                                ['sensors_config.number', '=', $alert->number],
                                ['sensors.visibility', '=', 'on'],
                                ['sensors.'.$alert->value_type.'_alert_activity', '=', 'on']
                            ])
                            ->get();
                foreach ($sensors as $key => $sensor) {
                    $this->stations[$sensor->stationId][] = $sensor;
                }
            }
            $this->prepareIndvLimits($users);
        }
    }
    /**
     * Update station mail send status
     *
     * @param $data
     * @param $table
     * @param $flag
     */
    private function updateStatus($data, $table, $flag = false) {
        if (!$flag) {
            // update error or alerts mail status
            foreach ($data as $row => $arr) {
                DB::table($table)
                    ->where('id', '=', $arr->id)
                    ->update(['status' => $this->now]);
            }
        } else {
            // update stations_config offline status
            foreach ($data as $key => $serial) {
                DB::table('stations_config')
                    ->where('serial_number', '=', $serial)
                    ->update(['status' => $this->now]);
            }
        }
    }
    /**
     * Prepare and send global limits
     *
     * @param $users
     */
    private function prepareGlobalLimits($users) {
        // get global limits
        $this->limits = $this->prepareData('global-limits');
        if ($this->alerts['global'] !== NULL && count($this->alerts['global']) > 0) {
            // send global
            $this->sendTo($users, 'alerts-global');
            // update alerts status
            $this->updateStatus($this->alerts['global'], 'detected_alerts');
            $this->alerts['global'] = NULL;
        }
    }
    /**
     * Prepare and send individual limits
     *
     * @param $users
     */
    private function prepareIndvLimits($users) {
        $all = [];
        foreach ($users as $key => $user) {
            $userStations = json_decode($user->visible_stations);
            if (in_array('all', $userStations)) {
                $all[] = $user;
                unset($users[$key]);
            }
        }
        if (count($all) > 0) {
            // send mail to users with all station visible
            $this->sendTo($all, 'indv-alerts-toAll');
            // update alerts status
            $this->updateStatus($this->alerts['individual'], 'detected_alerts');
        }
        // prepare users with stations
        $newUsers = [];
        foreach ($users as $key => $user) {
            $this->newStations = [];
            $newUsers = [];
            foreach ($this->stations as $stationId => $stationData) {
                $userStations = json_decode($user->visible_stations);
                if (in_array($stationId, $userStations)) {
                    $newUsers[] = $user;
                    $this->newStations[$stationId] = $stationData;
                }
            }
            if (count($newUsers) > 0 && count($this->newStations) > 0) {
                // send mails to rest users
                $this->sendTo($newUsers, 'indv-alerts-toUser');
            }
        }
        // update alerts status
        $this->updateStatus($this->alerts['individual'], 'detected_alerts');
        $this->alerts['individual'] = NULL;
    }
    /**
     * Send e-mail
     *
     * @param $emails
     * @param $type
     */
    private function sendTo($emails, $type) {
        foreach ($emails as $user) {
            // send emails 
            switch ($type) {
                case 'offline': {
                    dispatch(new MailingJob($type, $user->username, $this->company, $this->offlines));
                } break;
                case 'error': {
                    dispatch(new MailingJob($type, $user->username, $this->company, $this->errors));
                } break;
                case 'alerts-global': {
                    dispatch(new MailingJob($type, $user->username, $this->company, $this->alerts['global'], $this->limits));
                } break;
                case 'indv-alerts-toAll': {
                    dispatch(new MailingJob($type, $user->username, $this->company, $this->alerts['individual'], $this->stations, 'toAll'));
                } break;
                case 'indv-alerts-toUser': {
                    Mail::to($user->username)->send(new IndividualAlert($this->company, $this->alerts['individual'], $this->newStations, 'toAll'));
                    // dispatch(new MailingJob($type, $user->username, $this->company, $this->alerts['individual'], $this->newStations, 'toAll'));
                } break;
            }
        }
    }
}
