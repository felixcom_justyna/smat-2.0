<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CreateDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:database {dbName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new database for system';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'Name of database'],
        ];
    }

     /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::connection()->statement("CREATE DATABASE IF NOT EXISTS {$this->argument('dbName')}");

        return 0;
    }
}
