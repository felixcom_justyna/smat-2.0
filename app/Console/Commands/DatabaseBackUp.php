<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Config;

class DatabaseBackUp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup all smats data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->connections  = Config::get('database.connections');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (count($this->connections) > 0) {
            foreach ($this->connections as $dbName => $dbConfig) {
                if (str_contains($dbName, config('app.name'))) {
                    $filename = $dbName."-databackup-".date('Y-m-d').".gz";
  
                    $command = "mysqldump 
                    --user=".env('DB_USERNAME')." 
                    --password=".env('DB_PASSWORD')." 
                    --host=".env('DB_HOST').
                    " "
                    .$dbName." | gzip > ".storage_path()."/app/backup/".$filename;
              
                    $returnVar = NULL;
                    $output  = NULL;
              
                    exec($command, $output, $returnVar);
                }
            }
        }
    }
}
