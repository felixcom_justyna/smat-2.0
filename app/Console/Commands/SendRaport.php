<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

use App\Http\Helpers\Utilities;
use App\Http\Controllers\VirtualSensorController;
use App\Http\Controllers\RaportController;
use App\Models\VirtualSensor;
use App\Models\Station;
use App\Models\Sensor;
use App\Models\Smat;

use Illuminate\Support\Facades\Mail;
use App\Mail\Raport;

class SendRaport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'autoraport:everyminute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check autoraports, generate pdfs and send e-mail';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public $emails, $startTime, $frequency, $interval, $stations;
    public function __construct()
    {
        parent::__construct();
        $this->time = date('H:i').':00';
        $this->now = date('Y-m-d H:i');
        $this->connections = Config::get('database.connections');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() 
    {
        if (count($this->connections) > 0) {
            // remove pdfs
            $this->removeFiles();
            foreach ($this->connections as $dbName => $dbConfig) {
                if (str_contains($dbName, config('app.name').'_')) {
                    try {
                        // set database
                        config(['database.default' => $dbName]);
                        // get autoraport
                        $autoraports = DB::table('autoraports')
                                        ->select('*')
                                        ->where([
                                            ['sending_days', 'LIKE', '%'.strtolower(date('l')).'%'],
                                            ['sending_time', '=', $this->time]
                                        ])
                                        ->get();
                        if (count($autoraports) > 0) {
                            for ($i = 0; $i < count($autoraports); ++$i) {
                                //set data
                                (int)$this->interval = ($autoraports[$i]->interval == 1) ? '3600' : $autoraports[$i]->interval;
                                (int)$this->frequency = $autoraports[$i]->frequency;
                                $this->startTime = $autoraports[$i]->start_time;
                                $this->stations = json_decode($autoraports[$i]->stations);
                                $this->emails = $this->checkEmails(json_decode($autoraports[$i]->emails));
    
                                if ($autoraports[$i]->status != $this->now) {
                                    $this->prepareStatistics();
                                    // update sending status
                                    DB::table('autoraports')
                                        ->where('id', $autoraports[$i]->id)
                                        ->update(['status' => $this->now]);
                                }
                            }
                        }
                    } catch(\Exception $e){
                        Utilities::insertLog('error', 'SendRaport 92: '.$e->getMessage());
                        die();
                    }
                }
            }
        }
    }
    private function removeFiles() 
    {
        try {
            //get all pdfs
            $files = glob(public_path().'/raports/*.pdf');
            foreach($files as $file) { 
                if (is_file($file)) {
                    unlink($file); 
                }
            }
            return true;
        } catch (\Exception $e) {
            Utilities::insertLog('error', 'SendRaport 105: '.$e->getMessage());
            die();
        }
        return false;        
    }
    private function checkEmails($emails) 
    {
        foreach ($emails as $key => $email) {
            $exist = DB::table('users')
                        ->where([
                            ['username', $email],
                            ['auto_raport', 'off']
                        ])
                        ->first();
            if ($exist !== NULL) {
                unset($emails[$key]);
            }
        }
        return $emails;
    }
    private function prepareStatistics() 
    {
        //prepare dates
        $end = date('Y-m-d').' '.$this->startTime;
        $start = date('Y-m-d H:i', strtotime('-'.$this->frequency.' days', strtotime($end))).':00';
        // get station's serial number
        foreach ($this->stations as $key => $stationId) {
            $station = Station::find($stationId);
            $stationSensors = VirtualSensor::where('station_id', '=', $stationId)
                                            ->get();
            $stationData = [
                'id' => $station->id,
                'name' => $station->name,
                'localization' => $station->localization,
                'interval' => $this->interval,
                'statistics' => [
                    'datatimes' => null,
                    'sensors' => [],
                ]
            ];
            foreach ($stationSensors as $key => $stationSensor) {
                $sensor = Sensor::find($stationSensor->sensor_id);
                $smat = Smat::find($sensor->station_id);
                $statTable = $smat->serial_number;
                $sensorVals = json_decode($stationSensor->showing_values);
                if ($stationData['statistics']['datatimes'] === null) {
                    // get datetimes
                    $query = 'SELECT DATE_FORMAT(t.date, "%Y-%m-%d %H:%i") as "date" 
                            FROM (
                                SELECT date 
                                FROM `'.$statTable.'`
                                WHERE date BETWEEN "'.$start.'" AND "'.$end.'"  
                                UNION
                                SELECT date 
                                FROM tmp_time
                                WHERE date BETWEEN "'.$start.'" AND "'.$end.'"  
                            ) AS t
                            GROUP BY unix_timestamp(DATE_FORMAT(`t`.`date`, "%Y-%m-%d %H:%i")) 
                            DIV '.$this->interval.'  
                            ORDER BY `t`.`date`  DESC';
                    $datetimes = DB::select($query);
                    $datetimes = Utilities::getValues($datetimes, 'date');
                    $stationData['statistics']['datatimes'] = $datetimes;
                }
                // prepare cols for query
                $queryCols = '';
                $subQueryCols = '';
                $errors = [];
                foreach ($sensorVals as $key => $val) {
                    // prepare columns for query
                    $queryCols .= (isset($sensorVals[$key+1])) ? 't.'.$val.', ' : 't.'.$val;
                    $subQueryCols .= (isset($sensorVals[$key+1])) ? $val.', ' : $val;
                }
                // prepare statistics
                $query = 'SELECT '.$queryCols.' 
                        FROM (
                            SELECT date, '.$subQueryCols.' 
                            FROM `'.$statTable.'`
                            WHERE date BETWEEN "'.$start.'" AND "'.$end.'"
                            AND sensor_id = '.$sensor->number.'
                            UNION
                            SELECT date, '.$subQueryCols.'  
                            FROM tmp_time
                            WHERE date BETWEEN "'.$start.'" AND "'.$end.'"  
                        ) AS t
                    GROUP BY unix_timestamp(DATE_FORMAT(`t`.`date`, "%Y-%m-%d %H:%i")) 
                    DIV '.$this->interval.'  
                    ORDER BY `t`.`date`  DESC';
                $statsData = DB::select($query);
                // prepare sensor data
                $stationData['statistics']['sensors'][] = [
                    'virtualSensorName' => $stationSensor->name,
                    'virtualSensorLocation' => $stationSensor->location,
                    'sensorName' => $sensor->name,
                    'sensorLocation' => $sensor->location,
                    'smatSerial' => $smat->serial_number,
                    'number' => $sensor->number,
                    'data' => $statsData,
                    'errors' => $errors
                ];
            }
            $this->prepareFile($stationId, $stationData);
        }
        $this->sendRaport();
    }
    private function prepareFile(int $stationId, array $data)
    {
        $file = new RaportController('pdf', $stationId, true, $data);
    }
    private function sendRaport()
    {
        $filesToSend = glob(public_path().'/raports/*.pdf');
        Mail::to($this->emails)->send(new Raport($filesToSend));
    }
}
