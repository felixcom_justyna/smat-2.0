<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\CreateDatabase;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CreateDatabase::class
    ];

    protected function scheduleTimezone() {
        return 'Europe/Warsaw';
    }
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        // insert data
        $schedule->command('data:test')
        ->everyMinute();
        // ->emailOutputTo('jusbil700@gmail.com');

        // insert every minute to tmp_time db table
        $schedule->command('time:everyminute')
        ->everyMinute();

        // check raports to send
        $schedule->command('autoraport:everyminute')
        ->everyMinute();
        // ->emailOutputTo('jusbil700@gmail.com');

        // check errors & alerts
        $schedule->command('data:checker')
        ->everyThreeMinutes();
        // ->everyMinute()
        // ->emailOutputTo('jusbil700@gmail.com');

        // database backup
        $schedule->command('database:backup')
        ->everyFiveMinutes();
        // ->emailOutputTo('jusbil700@gmail.com');

        // ->yearly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
