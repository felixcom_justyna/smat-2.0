<?php

namespace App\Http\Controllers;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

use App\Http\Helpers\Utilities;

class SensorDataController extends Controller {

    public function index() 
    {
        $_GET['id'] = 1;
        $_GET['obj'] = 100;

        if (isset($_GET['serial']) && isset($_GET['id']) && isset($_GET['obj']) && isset($_GET['data']) && count($_GET['data']) > 0) {

            $arr = $this->toHex($_GET['data']);
            $temp = ( ($arr[9] << 8) + ($arr[8] - 1000) ) / 10.0;
            $humi = ( ($arr[11] << 8) + $arr[10] ) / 10.0;

            $this->data = [
                'obj'       => (int)$_GET['obj'],
                'serial'    => $_GET['serial'],
                'sensorId'  => (int)$_GET['id'],
                'time'      => date("Y-m-d H:i:s"),
                'vals'      => [
                    'temp'  => (float)$temp,
                    'humi'  => (float)$humi,
                    'press' => NULL
                ]
            ];

            $this->table    =   ($this->data['serial'] !== null) ? config('app.name') . $this->data['serial'] : null;
            $this->db       =   config('app.name') . '_' . $this->data['obj'];

            $this->handle();
        }
    }

    private function handle()
    {
        try {
            // check stations config
            $isCorrect = DB::connection($this->db)
                            ->table('stations_config')
                            ->where('serial_number', $this->table)
                            ->first();
            if ($isCorrect !== null) {
                // get calibration
                $calibration = DB::connection($this->db)
                                ->table('sensors_config')
                                ->select('*')
                                ->where('number', '=', $this->data['sensorId'])
                                ->first();
                // insert data
                DB::connection($this->db)
                    ->table($this->table)
                    ->insert([
                        'date'      => $this->data['time'],
                        'sensor_id' => $this->data['sensorId'],
                        'temp'      => $this->data['vals']['temp'] + (float)$calibration->temp_diff,
                        'press'     => $this->data['vals']['press'] + (float)$calibration->press_diff,
                        'humi'      => $this->data['vals']['humi'] + (float)$calibration->humi_diff
                    ]
                );
                // check & prepare errors
                $errorsToInsert = $this->checkErrors($this->data['vals']);
                $this->insertError($errorsToInsert, $this->data['vals']);
                if (count($errorsToInsert) != 3) {
                    // check & prepare limits
                    $vals = [];
                    if (count($errorsToInsert) > 0) {
                        foreach ($this->data['vals'] as $key => $arr) {
                            if (!array_key_exists($key, $errorsToInsert)) {
                                $vals[$key] = $arr;
                            }
                        }
                    } else {
                        $vals = $this->data['vals'];
                    }
                    $this->checkLimits($vals);
                }
            }
        } catch(\Exception $e) {
            Utilities::insertLog('error', 'SensorDataController 84: '.$e->getMessage());
            die();
        }
    }

    private function checkLimits($vals) 
    {
        $globalToInsert = $this->checkGlobalLimits($vals);
        $indvToInsert   = $this->checkIndvLimits($vals);

        $this->insertAlert($globalToInsert, $this->data['vals'], 'global');
        $this->insertAlert($indvToInsert, $this->data['vals'], 'individual');
    }

    private function checkGlobalLimits($vals) 
    {
        $globalLimits = DB::connection($this->db)->table('alerts_limits')->get();
        foreach ($globalLimits as $limit) {
            if ($limit->status == 'on' && isset($vals[$limit->value])) {
                // check value
                $min    =   (float)$limit->min;
                $max    =   (float)$limit->max;
                $val    =   (float)$vals[$limit->value];
                if ($val > $min && $val < $max) {
                    unset($vals[$limit->value]);
                }
            } else if (isset($vals[$limit->value])) {
                unset($vals[$limit->value]);
            }
        }
        return $vals;
    }

    private function checkIndvLimits($vals) 
    {
        $indvLimits = DB::connection($this->db)
                        ->table('sensors')
                        ->join('stations', 'stations.id', '=', 'sensors.station_id')
                        ->join('sensors_config', 'sensors_config.id', '=', 'sensors.sensor_id')
                        ->join('stations_config', 'stations_config.id', '=', 'sensors_config.station_id')
                        ->select('sensors.*', )
                        ->where([
                            ['stations_config.serial_number', '=', config('app.name') . $this->data['serial']],
                            ['sensors_config.number', '=', $this->data['sensorId']],
                            ['stations.visibility', '=', 'on'],
                            ['sensors.visibility', '=', 'on'],
                        ])
                        ->get();
        $toInsert = [];
        for ($i = 0; $i < count($indvLimits); ++$i) {
            foreach ($vals as $key => $val) {
                if ($indvLimits[$i]->{$key . '_alert_activity'} == 'on') {
                    $min    =   (float)$indvLimits[$i]->{$key . '_min'};
                    $max    =   (float)$indvLimits[$i]->{$key . '_max'};
                    $val    =   (float)$val;
                    if ($val < $min || $val > $max) {
                        $toInsert[$key] = $val;
                    }
                }
            }
        }
        return $toInsert;
    }

    private function checkErrors($vals) 
    {
        foreach ($vals as $key => $val) {
            if ((int)$val != -8888 && (int)$val != -9999) {
                unset($vals[$key]);
            }
        }
        return $vals;
    }

    private function insertError($errors, $vals) 
    {
        foreach ($vals as $key => $val) {
            if (array_key_exists($key, $errors)) {
                // if error exist
                $opened = DB::connection($this->db)
                        ->table('detected_errors')
                        ->where([
                            ['serial_number', '=', config('app.name').$this->data['serial']],
                            ['sensor_id', '=', $this->data['sensorId']],
                            ['value_type', '=', $key],
                            ['end_date', '=', NULL]
                        ])->get();
                if (count($opened) > 0) {
                    //this error continues
                    continue;
                } else {
                    // insert new error
                    DB::connection($this->db)
                        ->table('detected_errors')
                        ->insert([
                            'serial_number' => config('app.name').$this->data['serial'],
                            'sensor_id'     => $this->data['sensorId'],
                            'value_type'    => $key,
                            'error_type'    => $val,
                            'date'          => $this->data['time']
                        ]);
                }
            } else {
                // if error doesn't exist, close opened statuses
                DB::connection($this->db)
                    ->table('detected_errors')
                    ->where([
                        ['serial_number', '=', config('app.name').$this->data['serial']],
                        ['sensor_id', '=', $this->data['sensorId']],
                        ['value_type', '=', $key],
                        ['end_date', '=', NULL],
                    ])
                    ->update([
                        'end_date'  => $this->data['time'],
                        'status'    => NULL
                    ]);
            }
        }
    }

    private function insertAlert($alerts, $vals, $flag) 
    {
        foreach ($vals as $key => $val) {
            if ($val !== NULL && array_key_exists($key, $alerts)) {
                // if alert exist
                $opened = DB::connection($this->db)
                            ->table('detected_alerts')
                            ->where([
                                ['serial_number', '=', config('app.name').$this->data['serial']],
                                ['sensor_id', '=', $this->data['sensorId']],
                                ['value_type', '=', $key],
                                ['alert_type', '=', $flag],
                                ['end_date', '=', NULL]
                            ])->get();
                if (count($opened) > 0) {
                    //this alert continues
                    continue;
                } else {
                    // insert new alert
                    DB::connection($this->db)
                        ->table('detected_alerts')
                        ->insert([
                            'serial_number'     => config('app.name').$this->data['serial'],
                            'sensor_id'         => $this->data['sensorId'],
                            'value_type'        => $key,
                            'alert_type'        => $flag,
                            'measured_value'    => $val,
                            'date'              => $this->data['time']
                        ]);
                }
            } else {
                // if alert doesn't exist, close opened statuses
                DB::connection($this->db)
                    ->table('detected_alerts')
                    ->where([
                        ['serial_number', '=', config('app.name').$this->data['serial']],
                        ['sensor_id', '=', $this->data['sensorId']],
                        ['value_type', '=', $key],
                        ['alert_type', '=', $flag],
                        ['end_date', '=', NULL],
                    ])
                    ->update([
                        'end_date'  => $this->data['time'],
                        'status'    => NULL
                    ]);
            }
        }
    }

    private function toHex($string) {
        $i = 0;
        $m = 0;
        if (strlen($string) / 2 <> (int)(strlen($string) / 2)) {
            $string = "0" . $string;
        }
     
        $m = strlen($string) / 2;
        $abytes = array_fill(0, $m-1, 0);
        for ($i = 0; $i <= ($m-1); ++$i) {
            $raw = substr($string, $i * 2 , 2);
            $hexed = hexdec($raw);
            $abytes[$i] = $hexed;
        }
        return $abytes;
     }
}
