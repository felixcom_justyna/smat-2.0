<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Helpers\Utilities;
use App\Http\Controllers\VirtualSensorController;
use App\Models\VirtualSensor;
use App\Models\Station;
use App\Models\Sensor;
use App\Models\Smat;

use DateTime;
use Session;

class StationController extends Controller 
{
    public function getData() 
    {
        $stations = Station::all();
        foreach ($stations as $station) {
            //get virtual sensors
            $sensors = new VirtualSensorController();
            $sensors = $sensors->getSensors($station->id);
            // get current vals for sensor
            foreach ($sensors as $sensor) {
                $table          =   $sensor->serialNumber;
                $sensorNumber   =   $sensor->mainSensorsNumber;
                $settings       =   DB::table($table)
                                        ->select('*')
                                        ->where('sensor_id', $sensorNumber)
                                        ->orderByDesc('date')
                                        ->limit(1)
                                        ->get();
                $sensor->settings = $settings;
            }            
            $station['sensors'] = $sensors;
        }
        $sensors = new SensorController();
        $data = [
            'stations'      =>  $stations,
            'mainSensores'  =>  $sensors->getAllSensors(),
        ];
        return $data;
    }

    public function getStation($id) {
        $station =   Station::findOrFail($id);
        $sensors = new VirtualSensorController();
        $sensors = $sensors->getSensors($id);
        $data = [
            'station'   =>  $station,
            'sensors'   =>  $sensors
        ];

        return $data;
    }
    public function getAllStations() {
        $stations  =   Station::all();
        $data = [];
        for ($i = 0; $i < count($stations); ++$i) {
            $sensors    =   new VirtualSensorController();
            $sensors    =   $sensors->getSensors($stations[$i]['id']);
            $data[]     =   [
                                'station'   => $stations[$i],
                                'sensors'   =>  $sensors
                            ];
        }
        return $data;
    }
    public function addStation(Request $request) {
        if (!$request->has('sensors')) {
            return redirect()->back()->with('error', 'Stanowisko musi posiadać przynajmniej jeden czujnik');
        } else {
            $station                        =   new Station();
            $station->name                  =   trim(request('name'));
            $station->location              =   trim(request('location'));
            $station->visibility            =   ($request->has('visibility')) ? 'on' : 'off';
            $station->sending_alert_raport  =   ($request->has('sendAlerts')) ? 'on' : 'off';
            if (strlen($station->name) == 0) {
                return redirect()->back()->with('error', 'Nazwa stanowiska nie może być pusta');
            } else {
                $station->save();
                $sensors = request('sensors');
                for ($i = 1; $i <= count($sensors); ++$i) {
                    $vals = [];
                    if (count($sensors[$i]['vals']) == 0) {
                        $vals = ['temp'];
                    } else {
                        foreach ($sensors[$i]['vals'] as $key => $val) {
                            array_push($vals, $key);
                        }
                    }
                    try {
                        VirtualSensor::insert([
                            'name'              => $sensors[$i]['name'],
                            'location'          => $sensors[$i]['location'],
                            'showing_values'    => json_encode($vals),
                            'visibility'        => (isset($sensors[$i]['visibility'])) ? 'on' : 'off',
                            'station_id'        => $station->id,
                            'sensor_id'         => $sensors[$i]['sensorId'],
                        ]);
                    } catch (\Exception $e) {
                        Utilities::insertLog('error', 'StationController 120 : '.$e->getMessage());
                        return redirect()->back()->with('error', 'Nie udało zapisać się zmian. Spróbuj ponownie później');
                    }
                }
                Utilities::insertLog('notice', 'Dodano nowe stanowisko pomiarowe, o nazwie: '.$station->name);
                return redirect()->back()->with('success', 'Poprawie dodano nowe stanowisko pomiarowe');
            }
        }
    }
    public function editStation(Request $request) {
        if (!$request->has('sensors')) {
            return redirect()->back()->with('error', 'Stanowisko musi posiadać przynajmniej jeden czujnik');
        } else {
            try {
                // edit station data
                Station::where('id', request('station'))
                ->update([
                    'name'                  => trim(request('stationName')),
                    'location'              => trim(request('stationLocation')),
                    'visibility'            => $request->has('visibility') ? 'on' : 'off',
                    'sending_alert_raport'  => $request->has('sendAlerts') ? 'on' : 'off'
                ]);
                $reqSensors =   request('sensors');
                //get all station's sensors
                $sensors    =   new VirtualSensorController;
                $sensors    =   $sensors->getData((int)request('station'));
                //check which doesn't exist
                foreach($sensors as $key => $val) {
                    if (array_key_exists((int)$sensors[$key]->id, $reqSensors)) {
                        $toUpdate   =   $reqSensors[(int)$sensors[$key]->id];
                        $vals       =   [];
                        if (count($toUpdate['vals']) == 0) {
                            $vals = ['temp'];
                        } else {
                            foreach ($toUpdate['vals'] as $k => $v) {
                                array_push($vals, $k);
                            }
                        }
                        //if exist, update
                        $update = new VirtualSensorController;
                        $update->updateSensor((int)$sensors[$key]->id, $toUpdate, $vals);
                    } else {
                        //delete if doesn't exist
                        $delete = new VirtualSensorController;
                        $delete->deleteSensor((int)$sensors[$key]->id);
                    }
                }
                //add new
                foreach($reqSensors as $key => $sensor) {
                    if (strpos($key, 'new_') !== false) {
                

                        $vals = [];
                        if (count($sensor['vals']) == 0) {
                            $vals = ['temp'];
                        } else {
                            foreach ($sensor['vals'] as $k => $v) {
                                array_push($vals, $k);
                            }
                        }
                        DB::table('sensors')->insert([
                            'name'              => $sensor['name'],
                            'location'          => $sensor['location'],
                            'showing_values'    => json_encode($vals),
                            'visibility'        => (isset($sensor['visibility'])) ? 'on' : 'off',
                            'station_id'        => (int)request('station'),
                            'sensor_id'         => (int)str_replace('new_', '', $key),
                        ]);
                    }
                }
                //save log
                Utilities::insertLog('notice', 'Edytowano stanowisko pomiarowe, o nazwie: '.request('stationName'));
                //send status
                return redirect()->back()->with('success', 'Zmiany zostały poprawnie zapisane');
              } catch (\Exception $e) {
                    Utilities::insertLog('error', 'StationController 202 : '.$e->getMessage());
                    return redirect()->back()->with('error', 'Nie udało zapisać się zmian. Spróbuj ponownie później');
              }
        }
    }
    public function deleteStation(Request $request) {
        $data       = json_decode($request->getContent(), true);
        $stationId  = (int)$data['params']['id'];
        try {
            // delete all station's sensors
            DB::table('sensors')->select('id')->where('station_id', $stationId)->delete();
            // get autoraports with station id
            $autoraports = DB::table('autoraports')->where('stations', 'like', '%"'.$stationId.'"%')->get();
            if (count($autoraports)  > 0) {
                // update autoraports
                for ($i = 0; $i < count($autoraports); ++$i) {
                    $stations   = json_decode($autoraports[$i]->stations);
                    $pos        = array_search((string)$stationId, $stations);
                    unset($stations[$pos]);
                    DB::table('autoraports')->where('id', (int)$autoraports[$i]->id)->update([
                        'stations'   => json_encode($stations)
                    ]);
                }
            }
            // delete station
            Station::findOrFail($stationId)->delete();
            // save log
            Utilities::insertLog('notice', 'Usunięto stanowisko pomiarowe');
            return true;
        } catch (\Exception $e) {
            Utilities::insertLog('error', 'StationController 230 : '.$e->getMessage());
            return false;
        }
    }
    public function getStatistics(int $stationId) {
        $station = Station::find($stationId);
        $stationSensors = VirtualSensor::where('station_id', '=', $stationId)
                                        ->get();
        $stationData = [
            'id' => $station->id,
            'name' => $station->name,
            'localization' => $station->localization,
            'statistics' => [
                'datatimes' => null,
                'sensors' => [],
            ]
        ];
        //prepare dates
        if (!session()->has('filters.startDate') &&
            !session()->has('filters.endDate')) {
            $start = date('Y-m-d H:i', strtotime("-".session('filters.days')." day", strtotime(date('Y-m-d H:i'))));
            $end = date('Y-m-d H:i');
        } else {
            $start = session('filters.startDate');
            $end = session('filters.endDate');
        }
        foreach ($stationSensors as $key => $stationSensor) {
            $sensor = Sensor::find($stationSensor->sensor_id);
            $smat = Smat::find($sensor->station_id);
            $statTable = $smat->serial_number;
            $sensorVals = json_decode($stationSensor->showing_values);
            if ($stationData['statistics']['datatimes'] === null) {
                // get datetimes
                $query = 'SELECT DATE_FORMAT(t.date, "%Y-%m-%d %H:%i") as "date" 
                        FROM (
                            SELECT date 
                            FROM `'.$statTable.'`
                            WHERE date BETWEEN "'.$start.'" AND "'.$end.'"  
                            UNION
                            SELECT date 
                            FROM tmp_time
                            WHERE date BETWEEN "'.$start.'" AND "'.$end.'"  
                        ) AS t
                        GROUP BY unix_timestamp(DATE_FORMAT(`t`.`date`, "%Y-%m-%d %H:%i")) 
                        DIV '.session('filters.interval').'  
                        ORDER BY `t`.`date`  DESC';
                $datetimes = DB::select($query);
                $datetimes = Utilities::getValues($datetimes, 'date');
                $stationData['statistics']['datatimes'] = $datetimes;
            }
            // prepare cols for query
            $queryCols = '';
            $subQueryCols = '';
            $errors = [];
            foreach ($sensorVals as $key => $val) {
                // prepare columns for query
                $queryCols .= (isset($sensorVals[$key+1])) ? 't.'.$val.', ' : 't.'.$val;
                $subQueryCols .= (isset($sensorVals[$key+1])) ? $val.', ' : $val;
                // prepare errors 
                if (session('userData.user_type') == 'dev') {
                    $query = 'SELECT `date`
                            FROM '.$statTable.'
                            WHERE `sensor_id` = '.$sensor->number.' 
                            AND '.$val.' < 0
                            ORDER BY `date`  DESC
                            LIMIT 1';
                    $error = DB::select($query);
                    $errors[$val] = (isset($error[0])) ? $error[0]->date : null;
                }
            }
            // prepare statistics
            $query = 'SELECT '.$queryCols.' 
                    FROM (
                        SELECT date, '.$subQueryCols.' 
                        FROM `'.$statTable.'`
                        WHERE date BETWEEN "'.$start.'" AND "'.$end.'"
                        AND sensor_id = '.$sensor->number.'
                        UNION
                        SELECT date, '.$subQueryCols.'  
                        FROM tmp_time
                        WHERE date BETWEEN "'.$start.'" AND "'.$end.'"  
                    ) AS t
                GROUP BY unix_timestamp(DATE_FORMAT(`t`.`date`, "%Y-%m-%d %H:%i")) 
                DIV '.session('filters.interval').'  
                ORDER BY `t`.`date`  DESC';
            $statsData = DB::select($query);
            // prepare sensor data
            $stationData['statistics']['sensors'][] = [
                'virtualSensorName' => $stationSensor->name,
                'virtualSensorLocation' => $stationSensor->location,
                'sensorName' => $sensor->name,
                'sensorLocation' => $sensor->location,
                'smatSerial' => $smat->serial_number,
                'number' => $sensor->number,
                'data' => $statsData,
                'errors' => $errors
            ];
        }
        return $stationData;
    }
}
