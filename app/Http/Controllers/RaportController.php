<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ChartController;
use App\Http\Controllers\StationController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\GeneralSettingsController;

use Session;

class RaportController extends Controller {

    public $toDownload;
    /**
     * Prepare raport file
     *
     * @var string $type
     * @var int $stationId
     * false = generate for download, true = generate to send e-mail
     * @var bool $flag
     * data for statistics to send file in email
     * @var $data
     */
    public function __construct(string $type, int $stationId, bool $flag = false, $data = false) {
        $chart = new ChartController;
        if (!$flag) {
            $station = new StationController();
            $data = [
                'settings' => session('settings'),
                'statistics' => $chart->getStatistics($stationId),
                'station' => $station->getStatistics($stationId),
                'raportDate' => date('d/m/Y'),
                'startDate' => null,
                'endDate' => null
            ];
            if (count($data['statistics']['statistics']['datatimes']) > 0) {
                $data['startDate'] = date('d/m/Y H:m', strtotime(end($data['statistics']['statistics']['datatimes'])));
                $data['endDate'] = date('d/m/Y H:m', strtotime($data['statistics']['statistics']['datatimes'][0]));
            }
            $file = new FileController($type, $data);
            $this->toDownload = $file->destination;
        } else {
            $settings = new GeneralSettingsController;
            $settings = $settings->getData();
            if (count($settings) > 0) {
                $settings = $settings['data']['settings'];
            }
            $statsData = [
                'settings' => $settings,
                'statistics' => $chart->getStatistics($stationId, true, $data),
                'station' => $data,
                'interval' => $data['interval'],
                'raportDate' => date('d/m/Y'),
                'startDate' => null,
                'endDate' => null
            ];
            if (count($data['statistics']['datatimes']) > 0) {
                $statsData['startDate'] = date('d/m/Y H:m', strtotime(end($data['statistics']['datatimes'])));
                $statsData['endDate'] = date('d/m/Y H:m', strtotime($data['statistics']['datatimes'][0]));
            }
            $file = new FileController($type, $statsData, $flag);
        }
        
    }
}
