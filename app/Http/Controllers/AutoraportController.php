<?php

namespace App\Http\Controllers;

use App\Http\Helpers\Utilities;
use Illuminate\Http\Request;
use App\Models\Autoraport;
use App\Models\User;

use Session;

class AutoraportController extends Controller {

    public function getUsers() {
        $users = User::where('auto_raport', 'on')->get();
        return $users;
    }
    public function getData() {
        $raports = Autoraport::all();
        return ['data' => $raports];
    }
    public function getRaport($id) {
        $raport = Autoraport::findOrFail($id);
        return $raport;
    }
    public function addRaport(Request $request) {
        if ($request->has('emails') && $request->has('days') && $request->has('stations')) {
            try {
                $raport                 =   new Autoraport();
                $raport->emails         =   json_encode(request('emails'));
                $raport->sending_days   =   json_encode(request('days'));
                $raport->sending_time   =   trim(request('sending-time'));
                $raport->start_time     =   trim(request('start-time'));
                $raport->interval       =   (int)trim(request('interval'));
                $raport->frequency      =   (int)trim(request('frequency'));
                $raport->stations       =   json_encode(request('stations'));
                $raport->save();
                
                Utilities::insertLog('notice', 'Utworzono nowy auto-raport');
                return redirect()->back()->with('success', 'Pomyślnie zapisano nowy auto-raport');
             } catch(\Exception $e){
                Utilities::insertLog('error', $e->getMessage());
                return redirect()->back()->with('error', 'Wystąpił błąd. Spróbuj ponownie później');
             }
            
        } else {
            return redirect()->back()->with('error', 'Autoraport musi zawierać przynajmniej jeden adres e-mail, jedne stanowisko oraz jeden dzień do wysyłki');
        }
    }
    public function editRaport(Request $request) {
        if ($request->has('emails') && $request->has('days') && $request->has('stations')) {
            try {
                $editRaport = Autoraport::where('id', request('id'))
                                        ->update([
                                            'emails'        => json_encode(request('emails')),
                                            'sending_days'  => json_encode(request('days')),
                                            'sending_time'  => request('sending-time'),
                                            'start_time'    => request('start-time'),
                                            'interval'      => request('interval'),
                                            'frequency'     => request('frequency'),
                                            'stations'      => json_encode(request('stations'))   
                                        ]);
                Utilities::insertLog('notice', 'Edytowano auto-raport');
                return redirect()->back()->with('success', 'Zmiany zostały poprawnie zapisane');
            } catch(\Exception $e){
                Utilities::insertLog('error', $e->getMessage());
                return redirect()->back()->with('error', 'Wystąpił błąd. Spróbuj ponownie później');
            }
        } else {
            return redirect()->back()->with('error', 'Autoraport musi zawierać przynajmniej jeden adres e-mail, jedne stanowisko oraz jeden dzień do wysyłki');
        }
    }
    public function deleteRaport(Request $request) {
        $data       = json_decode($request->getContent(), true);
        $raportId   = (int)$data['params']['id'];
        // delete autoraport
        Autoraport::findOrFail($raportId)->delete();
        // save log
        Utilities::insertLog('notice', 'Usunięto autoraport');
        // send status
        return true;
    }
}
