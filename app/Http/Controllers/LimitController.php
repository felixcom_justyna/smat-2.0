<?php

namespace App\Http\Controllers;

use App\Models\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\Utilities;

class LimitController extends Controller {
    public function getGlobalLimits() {
        $data = ['globalLimits'  =>  Limit::all()];
        return ['data' => $data];
    }

    public function editLimits(Request $request) {
        if ($request->has('temp') && $request->has('humi') && $request->has('press')) {
            foreach ($request->all() as $key => $arr) {
                if ($key != '_token') {
                    $limits = Limit::where('value', $key)->update([
                        'min'       => ($arr['min'] === NULL) ? NULL : $arr['min'],
                        'max'       => ($arr['max'] === NULL) ? NULL : $arr['max'],
                        'status'    => (isset($arr['status'])) ? 'on' : 'off'
                    ]);
                }
            }
            // find all opened global alerts and close that
            $opened = DB::table('detected_alerts')->where([
                ['end_date', '=', NULL],
                ['alert_type', '=', 'global']
            ])->update([
                'end_date'  => date('Y-m-d H:i:s'),
                'status'    => NULL
            ]);
            //save log
            Utilities::insertLog('notice', 'Edytowano limity globalne');
            //send status
            return redirect()->back()->with('success', 'Zmiany zostały poprawnie zapisane');
        } else {
            Utilities::insertLog('error', 'LimitController 40: '.$e->getMessage());
            return redirect()->back()->with('error', 'Wystąpił błąd. Spróbuj ponownie później');
        }
    }

    public function editStationLimits(Request $request) {
        if ($request->has('all')) {
            // save values for all sensors
            foreach(request('all') as $key => $arr) {
                if (is_int($key)) {
                    try {
                        $stationId = $key;
                        $setLimits  =   DB::table('sensors')->where('station_id', $stationId)->update([
                            'temp_min'              => ($arr['temp']['min'] !== NULL)    ? $arr['temp']['min'] : NULL,
                            'temp_max'              => ($arr['temp']['max'] !== NULL)    ? $arr['temp']['max'] : NULL,
                            'humi_min'              => ($arr['humi']['min'] !== NULL)    ? $arr['humi']['min'] : NULL,
                            'humi_max'              => ($arr['humi']['max'] !== NULL)    ? $arr['humi']['max'] : NULL,
                            'press_min'             => ($arr['press']['min'] !== NULL)   ? $arr['press']['min'] : NULL,
                            'press_max'             => ($arr['press']['max'] !== NULL)   ? $arr['press']['max'] : NULL,
                            'temp_alert_activity'   => (isset($arr['temp']['status']))  ? 'on' : 'off',
                            'humi_alert_activity'   => (isset($arr['humi']['status']))  ? 'on' : 'off',
                            'press_alert_activity'  => (isset($arr['press']['status'])) ? 'on' : 'off',
                        ]);
                        // close detected individual alerts
                        $sensors = DB::table('sensors')->where('station_id', $stationId)->get();
                        $this->closeIndvLimits($sensors);
                    } catch (\Exception $e) {
                        Utilities::insertLog('error', 'LimitController 68: '.$e->getMessage());
                        return redirect()->back()->with('error', 'Nie udało zapisać się zmian. Spróbuj ponownie później');
                    }
                }
            }
            //save log
            Utilities::insertLog('notice', 'Zmieniono wszystkie progi szczególne dla stacji: '.request('station'));
            //send status
            return redirect()->back()->with('success', 'Zmiany zostały poprawnie zapisane');
        } else {
            foreach($request->all() as $key => $arr) {
                if ($key !== '_token' && is_array($arr)) {
                    $val = $key;
                    foreach($arr as $id => $vals) {
                        try {
                            $setLimits  =   DB::table('sensors')->where('id', $id)->update([
                                ''.$val.'_min'              => ($vals['min'] !== NULL)    ? $vals['min'] : NULL,
                                ''.$val.'_max'              => ($vals['max'] !== NULL)    ? $vals['max'] : NULL,
                                ''.$val.'_alert_activity'   => (isset($vals['status']))  ? 'on' : 'off',
                            ]);
                            // close detected indv alerts
                            // $this->closeIndvLimits(array(0 => $id), $val);
                        } catch (\Exception $e) {
                            Utilities::insertLog('error', 'LimitController 91: '.$e->getMessage());
                            return redirect()->back()->with('error', 'Nie udało zapisać się zmian. Spróbuj ponownie później');
                        }
                    }
                }
            }
            //save log
            Utilities::insertLog('notice', 'Zmieniono progi szczególne dla stacji: '.request('station'));
            //send status
            return redirect()->back()->with('success', 'Zmiany zostały poprawnie zapisane');
        }
    }

    // private function closeIndvLimits($sensors, $val) {
    //     foreach ($sensors as $sensor) {
    //         $id = (is_object($sensor)) ? $sensor->sensor_id : $sensor;
    //         $opened = DB::table('detected_alerts')->where([
    //             ['end_date', '=', NULL],
    //             ['alert_type', '=', 'individual'],
    //             ['value_type', '=', $val],
    //             ['sensor_id', '=', $id]
    //         ])->update([
    //             'end_date'  => date('Y-m-d H:i:s'),
    //             'status'    => NULL
    //         ]);
    //     }
    // }
}
