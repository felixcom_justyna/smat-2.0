<?php

namespace App\Http\Controllers;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Http\Helpers\Utilities;
use App\Models\VirtualSensor;

class VirtualSensorController extends Controller {

    public function getData($stationId) {
        $data = VirtualSensor::select('sensors.*')
                            ->where('sensors.station_id', $stationId)
                            ->get();
        return $data;
    }
    public function getSensors($stationId) {
        $sensors = VirtualSensor::select('sensors.id', 'sensors.name', 'sensors.location', 'sensors.showing_values', 
                                        'sensors.visibility', 'sensors.temp_alert_activity as tempStatus', 'sensors.temp_min as tempMin', 'sensors.temp_max as tempMax',
                                        'sensors.humi_alert_activity as humiStatus', 'sensors.humi_min as humiMin', 'sensors.humi_max as humiMax',
                                        'sensors.press_alert_activity as pressStatus', 'sensors.press_min as pressMin', 'sensors.press_max as pressMax',
                                        'sensors_config.id as mainSensorId', 'sensors_config.number as mainSensorsNumber', 
                                        'sensors_config.location as mainSensorsLocation', 'stations_config.serial_number as serialNumber')
                                    ->where('sensors.station_id', $stationId)
                                    ->join('sensors_config', 'sensors_config.id', '=', 'sensors.sensor_id')
                                    ->join('stations_config', 'sensors_config.station_id', '=', 'stations_config.id')
                                    ->get();
        return $sensors;
    }
    public function updateSensor($id, $data, $vals) {
        VirtualSensor::where('id', $id)
                    ->update([
                                'name'              => trim($data['name']),
                                'location'          => trim($data['location']),
                                'showing_values'    => json_encode($vals),
                                'visibility'        => isset($data['visibility']) ? 'on' : 'off'
                            ]
                    );
    }
    public function deleteSensor($id) {
        VirtualSensor::where('id', $id)
                        ->delete();
    }
}
