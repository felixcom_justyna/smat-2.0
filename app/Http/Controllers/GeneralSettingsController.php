<?php

namespace App\Http\Controllers;

use App\Models\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\Utilities;
use App\Models\GeneralSettings;
use Session;
use File;

class GeneralSettingsController extends Controller {
    
    public function getData() {
        $settings = GeneralSettings::all()->first();
        $data = ['settings' => $settings];

        return ['data' => $data];
    }

    public function editSettings(Request $request) {
        try {
            // edit general settings
            $csvStatus = $request->has('csv')  ? 'on' : 'off';
            $settings = GeneralSettings::where('id', 1)->update([
                'main_header'   => trim(request('mainHeader')),
                'second_header' => trim(request('secondHeader')),
                'company_name'  => trim(request('companyName')),
                'csv_status'    => $csvStatus
            ]);
            // change session vals
            session()->put('settings.mainHeader', trim(request('mainHeader')));
            session()->put('settings.secondHeader', trim(request('secondHeader')));
            session()->put('settings.companyName', trim(request('companyName')));
            session()->put('settings.csvStatus', $csvStatus);
            // check file existing
            if ($request->hasFile('companyLogo')) {
                //prepare new file
                $file           = $request->file('companyLogo');
                $desPath        = 'media/';
                if (getimagesize($file)[0] >= 200) {
                    $fileExt        = $file->extension();
                    $newFileName    = 'object_'.session('settings.object').'.'.$fileExt;
                    // find and delete old file
                    if (File::exists(public_path('media/'.$newFileName))) {
                        File::delete(public_path('media/'.$newFileName));
                    }
                    // move new file
                    $file->move($desPath, $newFileName);
                    // save in db
                    $settings = GeneralSettings::where('id', 1)->update([
                        'company_logo'  => $newFileName
                    ]);
                    // change session val
                    session()->put('settings.companyLogo', $newFileName);
                } else {
                    return redirect()->back()->with('error', 'Wybrany logotyp ma poniżej 200px szerokości.');
                }
            } else if (!$request->hasFile('companyLogo') && request('clear')) {
                // clear data in db
                $settings = GeneralSettings::where('id', 1)->update([
                    'company_logo'  => NULL
                ]);
            }

            Utilities::insertLog('notice', 'Edytowano ustawienia ogólne');
            return redirect()->back()->with('success', 'Zmiany zostały poprawnie zapisane');
        } catch (\Exception $e) {
            Utilities::insertLog('error', 'GeneralSettingsController 65 : '.$e->getMessage());
            return redirect()->back()->with('error', 'Nie udało zapisać się zmian. Spróbuj ponownie później');
        }
    }

    public function editCharts(Request $request) {
        try {
            $settings = GeneralSettings::where('id', 1)->update([
                'linechart_status'  => $request->has('lineChartStatus') ? 'on' : 'off',
                'piechart_status'   => $request->has('pieChartStatus')  ? 'on' : 'off',
                'chart_temp_color'  => request('tempColor'),
                'chart_press_color' => request('pressColor'),
                'chart_humi_color'  => request('humiColor')
            ]);

            Utilities::insertLog('notice', 'Edytowano dane wykresów');
            return redirect()->back()->with('success', 'Zmiany zostały poprawnie zapisane');
        } catch (\Exception $e) {
            Utilities::insertLog('error', 'GeneralSettingsController 49 : '.$e->getMessage());
            return redirect()->back()->with('error', 'Nie udało zapisać się zmian. Spróbuj ponownie później');
        }
    }
}
