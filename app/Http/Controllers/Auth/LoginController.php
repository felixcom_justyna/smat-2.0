<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Http\Controllers\Controller;
use App\Models\Dashboard;
use Illuminate\Support\Facades\DB;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Helpers\Utilities;

use Artisan;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request) {
        $data           =   $request->only('email', 'password', 'object');
        $dbName         =   config('app.name'). '_' . $data['object'];
        $connections    =   Config::get('database.connections');
        if (array_key_exists($dbName, $connections)) {
            // set database
            Config::set('DB_NAME', $dbName);
            Config::set('database.connections.mysql.database', $dbName);
            Config::set('DB_OBJECT', $data['object']);
            Config::set('database.default', $dbName);
            Artisan::call('config:clear');
            Artisan::call('cache:clear');
            DB::purge('mysql');
    
            if (Auth::attempt(['username'  => $data['email'], 'password'  => $data['password']])) {
                $user = Auth::user();
                // set session
                session([
                    'db' => $dbName,
                    'settings' => Dashboard::find(1),
                    'userData' => $user->only([
                        'id',
                        'username', 
                        'name', 
                        'surname', 
                        'phone_number',
                        'user_type',
                        'visible_stations',
                        'alert_raport',
                        'auto_raport',
                    ])
                ]);
                $user->logged = date("Y-m-d H:i:s");
                $user->save();
                Utilities::insertLog('info', 'Logowanie użytkownika: '. $user->username);

                return json_encode([
                    'user' => session('userData')['user_type'],
                    'path' => '/'
                ]);
            } 
        }
        return json_encode(['error' => 'Podano błędny e-mail lub hasło']);
    }
}
