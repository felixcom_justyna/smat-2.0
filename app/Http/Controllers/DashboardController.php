<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Dashboard;
use App\Http\Controllers\SmatController;
use Session;

class DashboardController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $this->setDefaultInfo();
        $stations = [];
        if ((session("userData")['user_type'] == 'dev')) {
            //check stations_configs table 
            $configs = DB::select('select * from `stations_config`');
            if (empty($configs)) {
                // redirect to panel to add stations boxes
                return redirect('/admin/smat/add')->with('error', 'Nie wykryto konfiguracji stacji pomiarowych. Dodaj stację aby poprawnie skonfigurować system '.config('app.name'));
            }
        }
        return redirect('/stations');
    }

    private function setDefaultInfo() {
        $settings = Dashboard::find(1);
        // set settings info
        session()->put('settings', [
            'object' => config('DB_OBJECT'),
            'mainHeader' => $settings['main_header'],
            'secondHeader' => $settings['second_header'],
            'companyName' => $settings['company_name'],
            'companyLogo' => $settings['company_logo'],
            'chartTempColor' => $settings['chart_temp_color'],
            'chartPressColor' => $settings['chart_press_color'],
            'chartHumiColor' => $settings['chart_humi_color'],
            'pieChartStatus' => $settings['piechart_status'],
            'lineChartStatus' => $settings['linechart_status'],
            'csvStatus' => $settings['csv_status']
        ]);
        // set charts settings
        session()->put('chartSettings', [
            'default'   => [
                'days'      =>  1, 
                'interval'  =>  3600 
            ],
            'user'  =>  [
                'startDate' =>  NULL,
                'endDate'   =>  NULL,
                'interval'  =>  NULL 
            ]
        ]);
        // set stations
        $stations = new SmatController;
        session()->put('stations', $stations->getData());
    }
}
