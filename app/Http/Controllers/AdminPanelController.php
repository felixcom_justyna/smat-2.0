<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

use App\Http\Helpers\Utilities;
use App\Mail\Test;
use Session;

class AdminPanelController extends Controller {
    public function getLogs($sort, $filters) {
        try {
            if ((session('userData')['user_type'] == 'dev')) {
                $clause = [];
            } else {
                $clause[] = ['type', '!=', 'error'];
            }
            $data = [
                'logs'      =>  NULL,
                'filters'   =>  NULL,
                'sort'      =>  NULL
            ];
            if ($filters != 'all' && $filters != null) {
                $arr = explode('&', $filters);
                if (!in_array('notice', $arr)) {
                    $clause[] = ['type', '!=', 'notice'];
                }
                if (!in_array('errors', $arr)) {
                    $clause[] = ['type', '!=', 'error'];
                } else if (session('userData')['user_type'] != 'dev') {
                    $clause[] = ['type', '!=', 'error'];
                }
                if (!in_array('info', $arr)) {
                    $clause[] = ['type', '!=', 'info'];
                }
            } else if ($filters != 'all') {
                return $data;
            }
            $data['filters']    =   $filters;
            $data['sort']       =   $clause;
            $data['logs']       =   DB::table('logs')
                                        ->select('id', 'date', 'type', 'user', 'content')
                                        ->where($clause)
                                        ->orderBy('date', $sort)
                                        ->paginate(10);
            return $data;
        } catch (\Exception $e) {
            Utilities::insertLog('error', $e->getMessage());
            return $data;
        }
    }

    public function sendEmailExample() {
        try {
            //send email with password
            Mail::to(session('userData')['username'])->send(new Test());
            // insert log
            Utilities::insertLog('notice', 'Przetestowano wysyłkę wiadomości e-mail');
            return redirect()->back()->with('success', 'Wiadomość została wysłana');
         }
         catch(\Exception $e){
            Utilities::insertLog('error', $e->getMessage());
            return redirect()->back()->with('error', 'Napotkano błąd. Spróbuj ponownie później');
         }
    }

    public function cleanDatabase(Request $request)
    {
        $datetime = $request->all();
        $endDate = $datetime['end-date-clean'];
        $endTime = $datetime['end-time-clean'];
        $toEnd = $endDate.' '.$endTime;

        $tables = DB::select('SHOW TABLES');
            foreach ($tables as $table) {
                $table = (array)$table;
                foreach ($table as $key => $tableName) {
                    if (str_contains($tableName, 'smat') || $tableName == 'logs') {
                        try {
                            DB::table($tableName)->where('DATE(date)', '<', $endDate)->delete(); 
                        } catch (\Exception $e) {
                            Utilities::insertLog('error', 'AdminPanelController 89: '.$e->getMessage());
                            return redirect()->back()->with('error', 'Wystąpił błąd podczas czyszczenia tabeli: '.strupper($tableName));
                        }
                    }
                }
            }
            Utilities::insertLog('info', 'Wyczyszczono stare rekordy z bazy danych.');
            return redirect()->back()->with('success', 'Czyszczenie przeprowadzono pomyślnie');
    }
}
