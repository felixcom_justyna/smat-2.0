<?php

namespace App\Http\Controllers;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Http\Helpers\Utilities;
use App\Models\Smat;
use App\Models\Sensor;
use App\Http\Controllers\SensorController;
use App\Http\Controllers\UserController;

use Illuminate\Support\Facades\Mail;
use App\Mail\Calibration;

class SmatController extends Controller {
    //prepare data
    public function getData($json = false) {
        $smats = [];
        $smatBoxes  = Smat::all();
        foreach ($smatBoxes as $smat) {
            $sensors    =   Sensor::select('*')
                            ->where('station_id', $smat->id)
                            ->get();
            $smats[] = [
                'config'    =>  $smat,
                'sensors'   =>  $sensors
            ];
        }
        $data = [
            'smats' => $smats,
        ];
        return $data;
    }
    public function getSmatBox($serialNumber) {
        $smat       =   Smat::where('serial_number', $serialNumber)
                            ->firstOrFail();
        $sensors    =   Sensor::select('*')
                                ->where('station_id', $smat->id)
                                ->get();
        $data = [
            'smat'      =>  $smat,
            'sensors'   =>  $sensors
        ];
        return $data;
    }
    // create new smatBox config
    public function addSmatStation(Request $request) {
        $serialNumber = trim(strtoupper(request('serialNumber')));
        if (!str_starts_with($serialNumber, config('app.name'))) {
            return redirect()->back()->with('error', 'Podany numer seryjny jest niezgodny z formatem');
        } else {
            $location = trim(request('location'));
            $calibration_date = trim(request('calibrationDate'));
            $calibration_days = trim(request('calibrationDays'));
            if (strlen($serialNumber) == 0 || strlen($location) == 0 || strlen($calibration_date) == 0 || strlen($calibration_days) == 0) {
                return redirect()->back()->with('error', 'Numer seryjny, lokalizacja oraz data wzorcowania nie może być puste!');
            }
            $smatBox =   new Smat();
            $smatBox->serial_number =   $serialNumber;

            $smatBox->location = $location;
            $smatBox->calibration_date = $calibration_date;
            $smatBox->calibration_days = $calibration_days;
            $smatBox->calibration_end_date = date('Y-m-d', strtotime($smatBox->calibration_date.' + '.$smatBox->calibration_days.' days'));
            $smatBox->calibration_info_sended = NULL;

            if (strlen($smatBox->location) == 0) {
                return redirect()->back()->with('error', 'Podano nieprawidłowną lokalizację');
            } else {
                try {
                    // save new smatBox
                    $smatBox->save();
                    // add smatBox sensors
                    for ($i = 1; $i <= (int)request('sensorsNumber'); ++$i) {
                        $sensor = new SensorController();
                        $sensor->insertSensor($i, $request->all(), $smatBox->id);
                    }
                    // create table for new smatBox
                    $this->createTable($smatBox);
                    // insert log
                    Utilities::insertLog('notice', 'Dodano nową stację pomiarową, numer seryjny: '.$serialNumber);
                    return redirect()->back()->with('success', 'Pomyślnie dodano nową stację pomiarową');
                 }
                 catch(\Exception $e){
                     if (strpos($e->getMessage(), 'Duplicate entry') !== false) {
                        return redirect()->back()->with('error', 'Podany numer seryjny już istnieje');
                     }
                    Utilities::insertLog('error', 'SmatController line: 74, error: '.$e->getMessage());
                 }
            }
        }
    }
    // edit smatBox config
    public function editSmatStation(Request $request) {
        try {

            $serialNumber = trim(request('serialNumber'));
            $location = trim(request('location'));
            $calibration_date = trim(request('calibrationDate'));
            $calibration_days = trim(request('calibrationDays'));
            if (strlen($serialNumber) == 0 || strlen($location) == 0 || strlen($calibration_date) == 0 || strlen($calibration_days) == 0) {
                return redirect()->back()->with('error', 'Numer seryjny, lokalizacja oraz data wzorcowania nie może być puste!');
            }
            // edit smat location
            $smatBox = Smat::where('serial_number', '=', $serialNumber)
                            ->firstOrFail();
            $smatBox->location = $location;
            $smatBox->calibration_date = $calibration_date;
            $smatBox->calibration_days = $calibration_days;
            $smatBox->calibration_end_date = date('Y-m-d', strtotime($smatBox->calibration_date.' + '.$smatBox->calibration_days.' days'));
            $smatBox->calibration_info_sended = NULL;
            $smatBox->save();
            // edit sensors
            foreach ($request->all() as $sensorId => $data) {
                if (is_numeric($sensorId) && Sensor::where('id', $sensorId)) {
                    //edit sensor -> unset infosended, count end date
                    Sensor::where('id', $sensorId)
                            ->update([
                                'temp_diff' => $data['diffTemp'],
                                'press_diff' => $data['diffPress'],
                                'humi_diff' => $data['diffHumi'] 
                            ]);
                }
            }
            Utilities::insertLog('notice', 'Edytowano stację pomiarową, numer seryjny: '.request('serialNumber'));
            return redirect()->back()->with('success', 'Zmiany zostały pomyślnie zapisane');
        }
        catch(\Exception $e){
            Utilities::insertLog('error', 'Wystąpił błąd podczas zapisu. Spróbuj ponownie później.');
            return redirect()->back()->with('error', 'SmatController line 108: ' .$e->getMessage);
        }
    }
    private function createTable($smatBox) {
        Schema::create($smatBox->serial_number, function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->timestamps();
            $table->string('workingTime', 255)->nullable();
            $table->string('imei', 255)->nullable();
            $table->string('ccid', 255)->nullable();
            $table->datetime('date')->unique()->useCurrent = true;
            $table->integer('sensor_id')->length(11);   
            $table->decimal('temp', 10,1);
            $table->decimal('press', 10,1);
            $table->decimal('humi', 10,1);
        });
    }
    public function sendCalibrationAlert($serial) {
        try {
            // get emails to devs
            $devs = new UserController;
            $devs = $devs->getDevs();
            $emails = [];
            foreach ($devs as $dev) {
                $emails[] = $dev['username'];
            }
            // send email
            Mail::to($emails)->send(new Calibration(session('settings')['companyName'], $serial));
            // edit smat calibration_info_sended
            $smatBox = Smat::where('serial_number', '=', $serial)
                            ->firstOrFail();
            $smatBox->calibration_info_sended = date('Y-m-d');
            $smatBox->save();
            // change session data
            session()->put('stations', $this->getData());
            
            Utilities::insertLog('notice', 'Wysłano zgłoszenie wzorcowania do serwisu. Stacja pomiarowa, numer seryjny: '.$serial);
            return true;
        }
        catch(\Exception $e){
            Utilities::insertLog('error', 'Wystąpił błąd podczas zgłoszenia serwisu. SmatController: 160');
            return false;
        }
    }
}
