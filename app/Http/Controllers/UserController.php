<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

use App\Models\User;
use App\Models\Station;
use App\Http\Helpers\Utilities;
use App\Mail\NewAccount;
use Session;

class UserController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function getUser($id) {
        $user = User::findOrFail($id);
        return $user;
    }

    public function getUsers() {
        $data = [
            'users' => User::select('*')
                            ->get()
        ];
        return $data;
    }
    public function getDevs() {
        $devs = User::select('*')
                    ->where('user_type', '=', 'dev')
                    ->get();
        return $devs;
    }
    public function changeUserData() {
        $user = User::find(session('userData.id'));
        if (trim(request('email') == $user->username)) {
            $user->name         =   trim(request('name'));
            $user->surname      =   trim(request('surname'));
            $user->phone_number =   trim(request('phone'));
            // check name & surname
            if (strlen($user->name) == 0 ||
                strlen($user->surname) == 0) {
                return redirect()->back()->with('error', 'Pole imię i nazwisko nie może być puste');
            } else {
                // check & change phone
                $user->phone_number = (strlen($user->phone_number) == 0) ? NULL : $user->phone_number;
                // change user data
                $user->save();
                // change session
                session()->put('userData.name', $user->name);
                session()->put('userData.surname', $user->surname);
                session()->put('userData.phoneNumber', $user->phone_number);

                return redirect()->back()->with('success', 'Dane osobowe zostały pomyślnie zapisane');
            }
        } else {
            return redirect('/logout')->with('error', 'Napotkano błąd. Nastąpiło automatyczne wylogowanie');
        }
    }
    //edit user password
    public function changePassword(Request $request) {
        // dd($request->all());
        $user = User::find(session('userData.id'));
        // prepare data
        $data = $request->only('password', 'new-pass', 'r-new-pass');
        // check current password
        if (Auth::attempt(['username'  => session('userData.username'), 'password'  => $data['password']])) {
             // check new & r-new pass
            if ($data['new-pass'] === $data['r-new-pass']) {
                // one big letter, one number and 8 length
                $re = "/^(?=1*?[a-z])(?=1*?[A-Z])(?=.*?[0-9]).{8,}$/i"; 
                if (preg_match($re, $data['new-pass'])) {
                    // change password
                    $user->password = Hash::make($data['new-pass']);
                    $user->save();
                    
                    return redirect('/logout');
                } else {
                    return redirect()->back()->with('error', 'Hasło musi zawierać przynajmniej jedną cyfrę, dużą literę i być długości min. 8 znaków');
                }
            } else {
                return redirect()->back()->with('error', 'Nowe hasła są z sobą niezgodne');
            }
        } else {
            return redirect()->back()->with('error', 'Podane hasło jest nieprawidłowe');
        }
    }
    //add user 
    public function addUser(Request $request) {
        $user   =   User::select('*')->where('username', trim($request->username))->get();
        // check if email exist
        if (count($user) != 0) {
            return redirect()->back()->with('error', 'Podany e-mail już istnieje w bazie');
        } else {
            $stations   = Station::all();
            $stations   = (count($stations) == count(json_decode(request('stations')))) ? ['all'] : json_decode(request('stations'));
            $newUser    = new User();

            $newUser->username          =   trim(request('username'));
            $newUser->name              =   trim(request('name'));
            $newUser->surname           =   trim(request('surname'));
            $newUser->user_type         =   request('userType');
            $newUser->phone_number      =   trim(request('phoneNumber'));
            $newUser->visible_stations  =   ($stations != null) ? json_encode($stations) : null;
            $newUser->alert_raport      =   ($request->has('alertRaport')) ? 'on' : 'off';
            $newUser->auto_raport       =   ($request->has('autoRaport')) ? 'on' : 'off';
            // check data
            if (strlen($newUser->username) == 0) {
                return redirect()->back()->with('error', 'Podano nieprawidłowy adres e-mail');
            } else {
                if ($newUser->user_type == 'admin') {
                    if (strlen($newUser->name) == 0 || strlen($newUser->surname) == 0) {
                        return redirect()->back()->with('error', 'Podano nieprawidłowe dane');
                    }
                }
            }
            //prepare password
            $newPass            =   Str::random(6) . random_int(120, 439) . Str::random(6);
            $newUser->password  =   Hash::make($newPass);
            try {
                // save new user
                $newUser->save();
                //send email with password
                Mail::to($newUser->username)->send(new NewAccount($newPass));
                // insert log
                Utilities::insertLog('notice', 'Dodano nowego użytkownika, e-mail: '.$newUser->username);
                return redirect()->back()->with('success', 'Pomyślnie dodano nowe konto');
             }
             catch(\Exception $e){
                Utilities::insertLog('error', $e->getMessage());
                return redirect()->back()->with('error', 'Napotkano błąd. Spróbuj ponownie później');
             }
            // add new user
            if (strlen($smatBox->location) == 0) {
                return redirect()->back()->with('error', 'Podano nieprawidłowną lokalizację');
            } else {
                try {
                    // save new smatBox
                    $smatBox->save();
                    // add smatBox sensors
                    for ($i = 1; $i <= (int)request('sensorsNumber'); ++$i) {
                        $sensor = new SensorController();
                        $sensor->insertSensor($i, $request->all(), $smatBox->id);
                    }
                    // create table for new smatBox
                    $this->createTable($smatBox);
                    // insert log
                    Utilities::insertLog('notice', 'Dodano nową stację pomiarową, numer seryjny: '.$serialNumber);
                    return redirect()->back()->with('success', 'Pomyślnie dodano nową stację pomiarową');
                 }
                 catch(\Exception $e){
                     if (strpos($e->getMessage(), 'Duplicate entry') !== false) {
                        return redirect()->back()->with('error', 'Podany numer seryjny już istnieje');
                     } else {
                         dd($e->getMessage());
                     }
                 }
            }
        }
        // dd($request->all());

    }
    //edit user 
    public function editUser(Request $request) {
        $user = User::find(request('id'));
        if ($user->username == request('username')) {
            $allStations                =   Station::all();
            $checkedStations            =   json_decode(request('stations'));
            if (count($checkedStations) == count($allStations)) {
                $checkedStations = ['all'];
            } else if (count($checkedStations) == 0) {
                $checkedStations = null;
            }
            $user->name                 =   trim(request('name'));
            $user->surname              =   trim(request('surname'));
            $user->phone_number         =   trim(request('phoneNumber'));
            $user->user_type            =   request('userType');
            $user->visible_stations     =   json_encode($checkedStations);
            $user->alert_raport         =   $request->has('alertRaport') ? 'on' : 'off';
            $user->auto_raport          =   $request->has('autoRaport')  ? 'on' : 'off';
            if ($user->save()) {
                Utilities::insertLog('notice', 'Zmieniono dane konta użytkownika, e-mail: '.$user->username);
                return redirect()->back()->with('success', 'Dane zostały pomyślnie zapisane');
            }
        } else {
            Utilities::insertLog('error', 'Wystąpił błąd podczas próby edycji konta użytkownika, e-mail: '.request('username'));
            return redirect()->back()->with('error', 'Napotkano błąd. Spróbuj ponownie później');
        }
    }
    //delete user
    public function deleteUser(Request $request) {
        $id   = request('params')['id'];
        $user = User::find($id);
        if ($user != null && $user->user_type != 'dev') {
            $username = $user->username;
            $user->delete();
            // save log
            Utilities::insertLog('notice', 'Usunięto konto użytkownika, e-mail: '.$username);
            if (Session::get('userData.id') == $id) {
                return json_encode(url("/logout"));   
            }
            return json_encode(true);
        } else {
            Utilities::insertLog('error', 'Wystąpił błąd podczas próby usunięcia użytkownika');
            return json_encode(false);
        }
    }
    public function setUserData($request) {
        $user           =   User::find(Auth::user()->id);
        $user->logged   =   date('Y-m-d H:i');
        $user->save();
        // set user data
        $request->session()->put('userData', [
            'id'                => Auth::user()->id,
            'username'          => Auth::user()->username,
            'name'              => Auth::user()->name,
            'surname'           => Auth::user()->surname,
            'phoneNumber'       => Auth::user()->phone_number,
            'userType'          => Auth::user()->user_type,
            'visibleStations'   => Auth::user()->visible_stations,
            'alertRaport'       => Auth::user()->alert_raport,
            'autoRaport'        => Auth::user()->auto_raport,
            'lastLogged'        => Auth::user()->logged,
        ]);
    }
}
