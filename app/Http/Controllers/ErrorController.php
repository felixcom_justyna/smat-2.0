<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Error;

class ErrorController extends Controller {

    public $now, $serial, $sensorId, $value, $error;

    public function __constructor($serial, $sensorId, $value, $error) {
        $this->now      = date("Y-m-d H:i");
        $this->serial   = $serial;
        $this->sensorId = $sensorId;
        $this->value    = $value;
        $this->error    = $error;
        $this->date     = $this->$now;
    }

    public function getActiveErrors() {
        $opened = Error::select('*')->whereNull('end_date')->get();
        return $opened;
    }

    public function insertError() {
        $newError = new Error();
        $newError->serial_number = $this->serial;
        $newError->sensor_id     = $this->sensorId;
        $newError->value_type    = $this->value;
        $newError->error_type    = $this->error;
        $newError->date          = $this->now;

        $newError->save();
        return $newError->id;
    }
}
