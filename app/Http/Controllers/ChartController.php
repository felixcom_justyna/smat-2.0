<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\Utilities;

use App\Models\Station;
use App\Models\VirtualSensor;
use App\Models\Sensor;
use App\Models\Smat;
use App\Models\Chart;

use DateTime;
use Session;
use DB;

class ChartController extends Controller {
    public function getData() {
        $filtrs = Chart::all();
        $data = ['filtrs' => $filtrs];

        return ['data' => $data];
    }

    public function editFiltrs(Request $request) {
        if ($request->has('interval') && $request->has('days')) {
            //off all
            Chart::query()->update(['status' => 'off']);
            $filtrs = Chart::all();
            foreach($filtrs as $key => $value) {
                if ($request->has($value->type) && isset($request->{$value->type}[$value->id])) {
                    if (isset($request->{$value->type}[$value->id]['status'])) {
                        $filtr = Chart::where('id', $value->id)->update([
                            'status' => 'on'
                        ]);
                    }
                }
            }
            Utilities::insertLog('notice', 'Edytowano filtry wykresów');
            return redirect()->back()->with('success', 'Zmiany zostały poprawnie zapisane');
        } else {
            return redirect()->back()->with('error', 'Włącz conajmniej jeden filtr z "zakres" oraz "pomiar co"');
        }
    }
    /**
     * Prepare statistics for php charts
     *
     * false = prepare for click in statistics, true = prepare for file to send in email
     * @var bool flag
     */
    public function getStatistics(int $stationId, bool $flag = false, $data = false) {
        $station = Station::find($stationId);
        $stationSensors = VirtualSensor::where('station_id', '=', $stationId)
                                        ->get();
        $stationData = [
            'id' => $station->id,
            'name' => $station->name,
            'localization' => $station->localization,
            'statistics' => [
                'datatimes' => null,
                'charts' => [
                    'temp' => [],
                    'humi' => [],
                    'press' => []
                ]
            ]
        ];
        //prepare dates
        if (!$flag) {
            if (!session()->has('filters.startDate') &&
                !session()->has('filters.endDate')) {
                $start = date('Y-m-d H:i', strtotime("-".session('filters.days')." day", strtotime(date('Y-m-d H:i'))));
                $end = date('Y-m-d H:i');
            } else {
                $start = session('filters.startDate');
                $end = session('filters.endDate');
            }
        } else {
            $start = date('Y-m-d H:i', strtotime(end($data['statistics']['datatimes'])));
            $end = date('Y-m-d H:i', strtotime($data['statistics']['datatimes'][0]));
        }
        $interval = (session()->has('filters.interval')) ? session('filters.interval') : $data['interval'];
        foreach ($stationSensors as $key => $stationSensor) {
            $sensor = Sensor::find($stationSensor->sensor_id);
            $smat = Smat::find($sensor->station_id);
            $statTable = $smat->serial_number;
            $sensorVals = json_decode($stationSensor->showing_values);
            if ($stationData['statistics']['datatimes'] === null) {
                // get datetimes
                $query = 'SELECT DATE_FORMAT(t.date, "%Y-%m-%d %H:%i") as "date" 
                        FROM (
                            SELECT date 
                            FROM `'.$statTable.'`
                            WHERE date BETWEEN "'.$start.'" AND "'.$end.'"  
                            UNION
                            SELECT date 
                            FROM tmp_time
                            WHERE date BETWEEN "'.$start.'" AND "'.$end.'"  
                        ) AS t
                        GROUP BY unix_timestamp(DATE_FORMAT(`t`.`date`, "%Y-%m-%d %H:%i")) 
                        DIV '.$interval.'  
                        ORDER BY `t`.`date`  DESC';
                $datetimes = DB::select($query);
                $datetimes = Utilities::getValues($datetimes, 'date');
                $stationData['statistics']['datatimes'] = $datetimes;
            }
            foreach ($sensorVals as $key => $val) {
                // prepare sensor data
                $stationData['statistics']['charts'][$val][] = [
                    'virtualSensorName' => $stationSensor->name,
                    'virtualSensorLocation' => $stationSensor->location,
                    'sensorName' => $sensor->name,
                    'sensorLocation' => $sensor->location,
                    'sensorNumber' => $sensor->number,
                    'smatSerial' => $smat->serial_number,
                    'number' => $sensor->number,
                    'data' => []
                ];
                // prepare statistics
                $query = 'SELECT t.'.$val.' 
                                FROM (
                                    SELECT date, '.$val.' 
                                    FROM `'.$statTable.'`
                                    WHERE date BETWEEN "'.$start.'" AND "'.$end.'"  
                                    AND sensor_id = '.$sensor->number.'
                                    UNION
                                    SELECT date, '.$val.'  
                                    FROM tmp_time
                                    WHERE date BETWEEN "'.$start.'" AND "'.$end.'"  
                                ) AS t
                            GROUP BY unix_timestamp(DATE_FORMAT(`t`.`date`, "%Y-%m-%d %H:%i")) 
                            DIV '.$interval.'  
                            ORDER BY `t`.`date`  DESC';
                $statsData = DB::select($query);
                // push data 
                foreach ($statsData as $row) {
                    $index = count($stationData['statistics']['charts'][$val])-1;
                    $stationData['statistics']['charts'][$val][$index]['data'][] = ($row->{$val} === null || (float)$row->{$val} < 0) ? null : (float)$row->{$val};
                }
            }
            // remove empty vals
            $stationData['statistics']['charts'] = array_filter($stationData['statistics']['charts'], fn($data) => count($data) > 0);
        }
        return $stationData;
    }

    public function setCurrentFiltrs($days, $interval) {
        $setDefault = function($days, $interval) {
            if (!$days) {
                session()->put('filters.days', 1);
            }
            if (!$interval) {
                session()->put('filters.interval', 3600);
            }
        };
        if (session('filters') === null) {
            $setDefault($days, $interval);
        } else {
            if (strpos($days, '-') !== false) {
                // end and start date is set
                $dates = explode('TO', $days);
                $startDate = $dates[0];
                $endDate = $dates[1];
                // check if dates are correct
                if (DateTime::createFromFormat('Y-m-d H:i', $startDate) !== false &&
                    DateTime::createFromFormat('Y-m-d H:i', $endDate) !== false) {
                    if (strtotime($startDate) < strtotime($endDate)) {
                        Session::forget('filters.days');
                        session()->put('filters.startDate', $startDate); 
                        session()->put('filters.endDate', $endDate); 
                        session()->put('filters.interval', $interval);
                    } else {
                        abort(404);
                    }
                } else {
                    abort(404);
                }
            } else {
                if (session()->has('filters.endDate')) {
                    Session::forget('filters.endDate');
                }
                if (session()->has('filters.startDate')) {
                    Session::forget('filters.startDate');
                }
                $setDefault($days, $interval);
                if ($days !== false && $interval !== false) {
                    session()->put('filters.days', (int)$days);
                    session()->put('filters.interval', $interval);
                }
            }
        }
     }
}
