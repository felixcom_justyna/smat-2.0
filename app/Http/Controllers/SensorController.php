<?php

namespace App\Http\Controllers;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Http\Helpers\Utilities;
use App\Models\Sensor;

class SensorController extends Controller {
    public function insertSensor($num, $request, $smatId) {
        $this->sensor = new Sensor();
        $this->sensor->location             =   trim($request['location_'.$num]);
        $this->sensor->number               =   $num;
        // $this->sensor->calibration_date     =   $request['calibrationDate_'.$num];
        // $this->sensor->calibration_days     =   (int)$request['calibrationDays_'.$num];
        // $this->sensor->calibration_end_date =   date('Y-m-d', strtotime($request['calibrationDate_'.$num].' + '.$request['calibrationDays_'.$num].' days'));
        $this->sensor->temp_diff            =   (float)$request['diffTemp_'.$num];
        $this->sensor->humi_diff            =   (float)$request['diffHumi_'.$num];
        $this->sensor->press_diff           =   (float)$request['diffPress_'.$num];
        $this->sensor->station_id           =   (int)$smatId;

        $this->sensor->save();
    }
    public function getAllSensors() {
        $sensors = Sensor::join('stations_config', 'sensors_config.station_id', '=', 'stations_config.id')
                            ->select('sensors_config.*', 'stations_config.id as station_config_id', 'stations_config.serial_number', 'stations_config.location as station_location')
                            ->get();
        return $sensors;
    }
}
