<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\Utilities;

use PDF;
use Amenadiel\JpGraph\Graph;
use Amenadiel\JpGraph\Plot;

class FileController extends Controller {

    public $fileType, $data, $destination, $flag;
    /**
     * Prepare pdf or csv file
     *
     * @var string $fileType
     * @var int $stationId
     * false = click in statistics, true = generate for email
     * @var int $flag
     */
    public function __construct(string $fileType, array $data, bool $flag = false) {
        $this->fileType = $fileType;
        $this->data = $data;
        $this->flag = $flag;

        $this->prepareFile();
    }
    private function prepareFile() {
        $filePath = public_path().'/raports/';
        $fileName = 'SMAT - Raport danych środowiskowych z dnia '.date('Y-m-d').'_'.rand(1, 5689);
        $this->destination = $filePath . $fileName . '.'. $this->fileType;
        if ($this->fileType == 'pdf') {
            // prepare graphs imgs for pdf
            if ($this->prepareGraphs()) {
                $pdf = new \Elibyy\TCPDF\TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                // set config for pdf
                $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                $pdf->SetCreator(PDF_CREATOR);
                $pdf->SetAuthor('FelixCom nowoczesne technologie');
                $pdf->SetSubject('Raport SMAT - system pomiaru parametrów środowiskowych');
                $pdf->SetKeywords('pomiary, felixcom, smat, system pomiarów');

                $pdf->SetMargins(PDF_MARGIN_LEFT, 3, PDF_MARGIN_RIGHT);

                $pdf->SetPrintHeader(false);  
                $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
                
                $pdf->SetFont('freeserif', '', 8);
                $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
                $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                $pdf->SetCellPadding(0);

                $pdf->AddPage();
                
                $pdf->writeHTML(view('pdf.raport', $this->data)->render());
                $pdf->lastPage();
                $pdf->Output($this->destination, 'F');
            }
        } else if ($this->fileType == 'csv' && session('settings')['csvStatus'] == 'on') {
            // prepare csv
            $this->exportCsv($fileName);
            die;
        }
    }

    private function prepareGraphs() {
        try {
            $preparedGraphs   = [];
            $graphs = (isset($this->data['statistics']['statistics']['charts'])) ? 
                        $this->data['statistics']['statistics']['charts'] : 
                        $this->data['statistics']['charts']; 
            foreach ($graphs as $key => $data) {
                // prepare min & max vals
                $xMin = Utilities::findValue($data, 'min');
                $xMax = Utilities::findValue($data, 'max');
                if ($xMax !== NULL || $xMin !== NULL) {
                    $xMin = ($xMin - 20);
                    $xMax = ($xMax + 20);
                }
                // prepare labels
                $labels = (isset($this->data['statistics']['statistics']['datatimes'])) ? 
                        $this->data['statistics']['statistics']['datatimes'] : 
                        $this->data['statistics']['datatimes'];
                if (count($labels) <= 25) {
                    $labels = Utilities::changeDateFormat($labels, 'time');
                } else if (count($labels) > 25 && count($labels) <= 168) {
                    $labels = Utilities::changeDateFormat($labels, 'date');
                } else {
                    $labels = NULL;
                }
                if ($xMin !== NULL && $xMax !== NULL) {
                    // prepare colors
                    $shadowsNumber = (isset($this->data['statistics']['statistics']['charts'][$key])) ? 
                                    count($this->data['statistics']['statistics']['charts'][$key]) : 
                                    count($this->data['statistics']['charts'][$key]);
                    if (isset($this->data['settings']['chart'.ucfirst($key).'Color'])) {
                        $colors = Utilities::prepareShadows($this->data['settings']['chart'.ucfirst($key).'Color'], $shadowsNumber);
                    } else {
                        $colors = Utilities::prepareShadows($this->data['settings']->{'chart_'.$key.'_color'}, $shadowsNumber);
                    }
                    // create graph
                    $graph = $this->createGraph($labels, $xMin, $xMax);
                    // add sensors data as line
                    for ($i = 0; $i < count($data); ++$i) {
                        $line = new Plot\LinePlot($data[$i]['data']);
                        // prepare sensor name for graph legend
                        if ($data[$i]['virtualSensorName'] !== NULL && $data[$i]['virtualSensorName'] != '') {
                            $sensorName = $data[$i]['virtualSensorName'];
                        } else if ($data[$i]['sensorName'] !== NULL && $data[$i]['sensorName'] != '') {
                            $sensorName = $data[$i]['sensorName'];
                        } else {
                            $sensorName = $data[$i]['smatSerial'].', czujnik nr. '.$data[$i]['number'];
                        }
                        $line->SetLegend($sensorName); 
                        $line->SetColor($colors[$i]);
                        if (count($line->coords) == 1) {
                            $line->SetCenter();
                        }
                        $graph->Add($line);
                    }
                    if ($xMin !== NULL && $xMax !== NULL) {
                        // stream to png file
                        $pngName = $key. '_graph.png';
                        $pngHandler = $graph->Stroke(_IMG_HANDLER);
                        $pngName = public_path().'/media/graphs/'.$pngName;
                        $graph->img->Stream($pngName);
    
                        $preparedGraphs[] = $key;
                    }
                }
            }
            return true;
        }
        catch(\Exception $e) {
            dd($e);
            return false;
        }
    }

    private function createGraph($labels, $min, $max) {
        $min = intval(round($min));
        $max =  intval(round($max));

        $graph  = new Graph\Graph(1300, 350);
        $graph->SetScale("textlin",$min,$max);
        $graph->xscale->SetAutoTicks();
        $graph->graph_theme = NULL;
        $graph->img->SetAntiAliasing(false);
        $graph->SetBox(false);
        $graph->SetMargin(40, 10, 50, 80);
    
        $graph->yaxis->SetPos('min');
        $graph->yaxis->HideZeroLabel();
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false,false);
    
        $graph->xgrid->Show();
        $graph->xgrid->SetLineStyle("solid");
    
        $graph->xaxis->SetPos('min');
        if ($labels !== NULL) {
            $graph->xaxis->SetTickLabels($labels);
        } else {
            $graph->xaxis->HideLabels();
        } 
        $graph->xaxis->SetLabelAngle(90);
        $graph->title->SetAlign("left");
        $graph->title->SetFont(FF_DV_SANSSERIF, FS_BOLD, 10);
        $graph->legend->SetPos(0.04, 0.02,'top','right');
        $graph->legend->SetColumns(10);
        $graph->legend->SetFrameWeight(1);
    
        return $graph;
    }

    private function exportCsv($fileName)
    {
        $convert = function($val) {
            return mb_convert_encoding($val, 'UTF-16LE', 'UTF-8');
        };
        $data = $this->data['statistics'];
        $header = $data['name'];
        $statistics = $data['statistics'];
        $datatimes = $statistics['datatimes'];
        $vals = $statistics['charts'];

        $mainHeader = [];
        $mainHeader[] = $header;
        $valsHeader[] = 'Data i godzina';
        foreach ($vals as $key => $sensors) {
            foreach ($sensors as $sensorData) {
                if (empty($sensorData['virtualSensorName'])) {
                    $mainHeader[] = $convert('Czujnik nr. '.$sensorData['sensorNumber']);
                } else {
                    $mainHeader[] = $convert($sensorData['virtualSensorName']);
                }
                switch ($key) {
                    case 'temp': { $key = 'Temperatura'; } break;
                    case 'humi': { $key = 'Wilgotność'; } break;
                    case 'press': { $key = 'Ciśnienie'; } break;
                }
                $valsHeader[] = $key;
            }
        }

        $data = [
            $mainHeader,
            $valsHeader
        ];

        $arr = [];
        $dataRow = [];
        foreach ($datatimes as $dateKey => $date) {
            $arr = [];
            $arr[] = $date;
            foreach ($vals as $valKey => $sensors) {
                foreach ($sensors as $sensorData) {
                    $val = ($sensorData['data'][$dateKey] != NULL) ? $convert($sensorData['data'][$dateKey]) : 'OFFLINE';
                    $arr[] = $val;
                }
            }
            $data[] = $arr;
        }
        
        header('Content-Encoding: UTF-8');
        header('Content-type: application/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename="'.$fileName.'.csv"');
        header('Pragma: no-cache');

        $fp = fopen('php://output', 'wb');
        foreach ($data as $line) {
            fputcsv($fp, $line);
        }
        fclose($fp);
    }
}
