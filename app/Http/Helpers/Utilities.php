<?php

namespace App\Http\Helpers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Models\Helper;
use Session;

class Utilities {
    
    static function insertLog(string $type, string $content) {
        try {
            DB::table('logs')->insert([
                'date'      => date('Y-m-d H:i:s'), 
                'user'      => session('userData')['username'],
                'type'      => $type,
                'content'   => $content
            ]);
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }
    //find min or max in arr
    static function findValue(array $vals, string $type) {
        // for pdf
        foreach ($vals as $key => $val) {
            $min = NULL;
            $max = NULL;
            if (isset($val['data']) && count($val['data']) > 0) {
                $withoutNulls = array_diff($val['data'], array(NULL));
                if (count($withoutNulls) > 0) {
                    if (${$type} === null) {
                        if ($type == 'min') {
                            $min = min($withoutNulls);
                        } else if ($type == 'max') {
                            $max = max($withoutNulls);
                        }
                    } else {
                        if ($type == 'min' && $min > min($withoutNulls)) {
                            $min = min($withoutNulls);
                        } else if ($type == 'max' && $max < max($withoutNulls)) {
                            $max = max($withoutNulls);
                        }
                    }
                }
            }
        }
        return ${$type};
    }
    //change datetime format
    static function changeDateFormat(array $data, string $format) {
        foreach ($data as $key => $val) {
            $data[$key] = ($format == 'time') ? date('H:m', strtotime($val)) : date('d/m/Y', strtotime($val));
        }
        return $data;
    }
    // prepare shadow from rgb color
    static function prepareShadows(string $baseColor, int $shadows) {
        $baseColor = str_replace('rgb(', '', $baseColor);
        $baseColor = str_replace(')', '', $baseColor);
        $baseColor = explode(', ', $baseColor);

        $colors = [];
        $r = (int)$baseColor[0];
        $g = (int)$baseColor[1];
        $b = (int)$baseColor[2];
        for ($i = 0; $i < $shadows; ++$i) {
            // create shadow
            $r += ($i > 0) ? 20 : 0;
            $g += ($i > 0) ? 20 : 0;
            $b += ($i > 0) ? 20 : 0;
            // fix vals
            if ($r > 255) { $r = 255; }
            if ($r < 0) { $r = 0; }
            if ($g > 255) { $g = 255; }
            if ($g < 0) { $g = 0; }
            if ($b > 255) { $b = 255; }
            if ($b < 0) { $b = 0; }

            $colors[] = array($r, $g, $b);
        }
        return $colors;
    }

    static function getValues($data, $key) {
        $arrData = array();
        foreach($data as $val) {
            $arrData[] = $val->{$key};
        }
        return $arrData;
    }
}