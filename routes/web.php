<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\SmatController;
use App\Http\Controllers\StationController;
use App\Http\Controllers\AdminPanelController;
use App\Http\Controllers\SensorController;
use App\Http\Controllers\ChartController;
use App\Http\Controllers\AutoraportController;
use App\Http\Controllers\RaportController;
use App\Http\Controllers\GeneralSettingsController;
use App\Http\Controllers\LimitController;

use App\Mail\NewAccount;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

<<<<<<< HEAD
=======
// insert data
Route::post('/handler', [SensorDataController::class, 'index']);

>>>>>>> b52119a3... fixed css
// auth
Auth::routes([
    //off user register
    'register' => false,
    // off password reset
    'reset' => false
]);
// dashboard
Route::middleware('auth')->prefix('/')->group(function() {
    // login
    Route::get('/login', function () { return view('login'); })->name('login');
    Route::post('/login', [LoginController::class, 'login']);
    Route::get('/logout', [LoginController::class, 'logout']);
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard.home');
    Route::get('/stations', function() {
        $limits = new LimitController;
        $data   = new StationController();
        return view('dashboard.stations',  ['data' => [
                                                'stations' => $data->getData(),
                                                'globalLimits' => $limits->getGlobalLimits()
                                        ]]);
    })->name('stations'); 
    Route::get('/alarms', function() { 
        $limits     = new LimitController;
        $stations   = new StationController;
        return view('dashboard.alarms',  ['data' => [
                                            'stations' => $stations->getAllStations(),
                                            'globalLimits' => $limits->getGlobalLimits()
                                        ]]);
    })->name('alarms');
    Route::get('/settings', function() { 
        $data = new UserController;
        return view('dashboard.settings',  ['data' => [
                                                'user' => $data->getUser(session('userData')['id']),
                                                'admins' => $data->getUsers()
                                        ]]);
    })->name('settings');
    Route::post('/settings/changeUserData', [UserController::class, 'changeUserData']);
    Route::post('/settings/changePassword', [UserController::class, 'changePassword']);
    // statistics
    Route::get('/statistics/{id}/{days?}/{interval?}', function($id, $days = false, $interval = false) { 
        $chart = new ChartController;
        $station = new StationController();
        $chart->setCurrentFiltrs($days, $interval);
        return view('dashboard.statistics', ['data' => [
                                                'chartsFilters' => $chart->getData(),
                                                'statistics' => $chart->getStatistics($id),
                                                'station' => $station->getStatistics($id),
                                                'settings' => session('settings'),
                                                'checkedFilters' => session('filters')
                                        ]]);
    })->name('statistics');
    // generate
    Route::get('/generate/raport/{id}/{type}', function($type, $id) {
        $raport = new RaportController($type, $id);
        // echo json_encode($raport->toDownload);
        return response()->download($raport->toDownload);
    }); 
    // send calibration mail
    Route::get('/dashboard/calibrationAlert/{serial}', function($serial) { 
        $smat = new SmatController;
        if ($smat->sendCalibrationAlert($serial)) {
            return redirect()->back()->with('success', 'Zgłoszenie serwisu zostało pomyślnie zgłoszone');
        } else {
            return redirect()->back()->with('error', 'Napotkano błąd. Spróbuj ponownie później');
        }
    });
});
// admin panel
Route::middleware(['auth', 'admin.role'])->prefix('admin')->group(function() {
    // start
    Route::get('/logs/{sort}/{filters?}', function($sort, $filters = null) {
        $data = new AdminPanelController();
        return view('admin.logs',  ['data' => $data->getLogs($sort, $filters)]);
    })->name('admin.logs'); 
    // smat
    Route::prefix('smat')->group(function() {
        Route::get('/list', function() {
            $data = new SmatController();
            return view('admin.smat.list', ['data' => $data->getData()]);
        })->name('admin.smat.list'); 
        Route::get('/add', function () { return view('admin.smat.add'); })->middleware(['auth', 'dev.role'])->name('admin.smat.add');
        Route::post('/add', [SmatController::class, 'addSmatStation']);
        Route::get('/settings/{serial}', function($serial) {
            $data = new SmatController();
            return view('admin.smat.settings', ['data' => $data->getSmatBox($serial)]);
        })->name('admin.smat.settings');
        Route::post('/edit', [SmatController::class, 'editSmatStation']);
    });
    //stations
    Route::prefix('station')->group(function() {
        Route::get('/list', function() {
            $data = new StationController();
            return view('admin.station.list', ['data' => $data->getData()]);
        })->name('admin.station.list'); 
        Route::get('/add', function() {
            $sensors = new SensorController();
            return view('admin.station.add', ['data' => ['sensors' => $sensors->getAllSensors()]]);
        })->name('admin.station.add');
        Route::post('/add', [StationController::class, 'addStation']);
        Route::get('/settings/{id}', function($id) {
            $station = new StationController();
            $sensors = new SensorController();
            return view('admin.station.settings', ['data' => [
                                                    'station' =>  $station->getStation($id),
                                                    'allSensors' =>  $sensors->getAllSensors()
                                                ]]);
        })->name('admin.station.settings'); 
        Route::post('/edit', [StationController::class, 'editStation']);
        Route::post('/delete', [StationController::class, 'deleteStation']);
    });
    // limits
    Route::prefix('limits')->group(function() {
        Route::get('/global', function() {
            $data = new LimitController();
            return view('admin.limits.global', $data->getGlobalLimits());
        })->name('admin.limits.global'); 
        Route::post('/edit', [LimitController::class, 'editLimits']);
        Route::get('/individual', function() {
            $stations   = new StationController();
            return view('admin.limits.individual', ['data' => $stations->getData()]);
        })->name('admin.limits.individual'); 
        Route::post('/individual/edit', [LimitController::class, 'editStationLimits']);
    });
    // raports
    Route::prefix('raports')->group(function() {
        Route::get('/list', function() {
            $data = new AutoraportController();
            return view('admin.raports.list', ['data' => $data->getData()]);
        })->name('admin.raports.list'); 
        Route::get('/add', function() {
            $users      = new AutoraportController();
            $stations   = new StationController();
            $filtrs     = new ChartController();
            $stations   = new StationController();
            $data = [
                'users'     =>  $users->getUsers(),
                'stations'  =>  $stations->getData(),
                'filtrs'    =>  $filtrs->getData(),
                'stations'  =>  $stations->getData()
            ];
            return view('admin.raports.add', ['data' => $data]);
        })->name('admin.raports.add'); 
        Route::post('/add', [AutoraportController::class, 'addRaport']);
        Route::post('/settings/delete', [AutoraportController::class, 'deleteRaport']);
        Route::get('/settings/{id}', function($id) {
            $users      = new UserController();
            $stations   = new StationController();
            $raports    = new AutoraportController();
            $filtrs     = new ChartController();
            $stations   = new StationController();
            $data = [
                'users'         =>  $users->getUsers(),
                'raports'       =>  $raports->getData(),
                'raportToEdit'  =>  $raports->getRaport($id),
                'stations'      =>  $stations->getData(),
                'filtrs'        =>  $filtrs->getData(),
                'stations'      =>  $stations->getData()
            ];
            return view('admin.raports.settings', ['data' => $data]);
        })->name('admin.raports.settings'); 
        Route::post('/edit', [AutoraportController::class, 'editRaport']);
    });
    // user
    Route::prefix('user')->group(function() {
        Route::get('/list', function() {
            $data = new UserController();
            return view('admin.user.list', ['data' => $data->getUsers()]);
        })->name('admin.user.list');
        Route::get('/add', function () { 
            $stations = new StationController();
            return view('admin.user.add', ['data' => $stations->getData()]);
        })->name('admin.user.add');
        Route::post('/add', [UserController::class, 'addUser']);
        Route::get('/edit/{id}', function($id) {
            $user      = new UserController();
            $stations  = new StationController();
            $data = [
                'user'     =>  $user->getUser($id),
                'stations' =>  $stations->getData()
            ];
            return view('admin.user.edit', ['data' => $data]);
        })->name('admin.user.edit'); 
        Route::post('/settings/edit', [UserController::class, 'editUser']);
        Route::post('/delete', [UserController::class, 'deleteUser']);
    });
    // settings
    Route::prefix('settings')->group(function() {
        Route::get('/charts', function() {
            $data = new GeneralSettingsController();
            return view('admin.settings.charts', $data->getData());
        })->name('admin.settings.charts'); 
        Route::post('/charts/edit', [GeneralSettingsController::class, 'editCharts']);
        Route::get('/filtrs', function() {
            $data = new ChartController();
            return view('admin.settings.filtrs', $data->getData());
        })->name('admin.settings.filtrs'); 
        Route::post('/filtrs/edit', [ChartController::class, 'editFiltrs']);
        Route::get('/general', function() {
            $data = new GeneralSettingsController();
            return view('admin.settings.general', $data->getData());
        })->middleware(['auth', 'dev.role'])->name('admin.settings.general'); 
        Route::post('/general/edit', [GeneralSettingsController::class, 'editSettings']);
    });
    // tests
    Route::get('/tests', function() {
        return view('admin.tests.index');
    })->name('admin.tests.index'); 
    Route::post('/tests/sendEmail', [AdminPanelController::class, 'sendEmailExample']);
    // clear database
    Route::get('/database', function() {
        return view('admin.database.index');
    })->name('admin.database.index'); 
    Route::post('/database/clean', [AdminPanelController::class, 'cleanDatabase']);
});

Route::fallback(function() {
    abort(404);
});

Auth::routes();
