@extends('layouts.auth.app')

{{-- @section('content')
<main>
    <!-- HEADER -->
    <header class="login-header">
        <img src="{{ asset('media/company-logo.png') }}" alt="smat">
        <h1 class="main-font">SYSTEM POMIARU PARAMETRÓW ŚRODOWISKOWYCH</h1>
    </header>
    <!-- END HEADER -->
    <!-- LOGIN FORM -->
    <div class="login-wrapper">
        <h2 class="decor-line main-font">Panel logowania</h2>
        <form class="login-form text-font" method="POST" action="{{ route('login') }}">
            @csrf
            <label for="object">Kod obiektu:</label>
            <span>
                <i class="fas fa-key"></i>
                <input type="text" id="object" name="objNum" placeholder="wpisz numer obiektu" value="500">
            </span>
            <label for="email">E-mail:</label>
            <span>
                <i class="fas fa-user"></i>
                <input type="text" id="email" name="email" placeholder="wpisz swój e-mail" value="justyna@felixcom.pl">
            </span>
            <label for="password">Hasło:</label>
            <span class="fail">
                <i class="fas fa-lock error"></i>
                <input class="warning-border" type="password" id="password" name="pass" placeholder="podaj hasło"  value="testowe20">
            </span>
            <input type="submit" class="btn s-btn default-btn" value="Zaloguj">
        </form> 
        <p class=" text-font login-status error warning">
            {{ session('error') }}
        </p>
    </div>
    <!-- END LOGIN FORM -->
</main>
@endsection --}}