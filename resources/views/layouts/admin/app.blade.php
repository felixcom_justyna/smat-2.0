<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ url('media/favicon-company.png') }}">

    <title>FelixCom SMAT - panel administracyjny</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- CSS -->
    <link rel="stylesheet" href="{{ url('css/animations.css') }}">
    <!-- FONTS -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans:wght@400;700&display=swap" rel="stylesheet">
    <!-- ICONS -->
    <script src="https://kit.fontawesome.com/2ab51abe82.js" crossorigin="anonymous"></script>
    {{-- VUE --}}
    <script defer src="/js/app.js"></script>
</head>
<body>
    <div id="app">
        @if (isset($data)) 
            <admin-panel 
                :user="{{ json_encode(session('userData')) }}" 
                :settings="{{ json_encode(session('settings')) }}" 
                :success="{{ json_encode(session('success')) }}"
                :error="{{ json_encode(session('error')) }}"
                :data="{{ json_encode($data) }}">
            </admin-panel>
        @else 
            <admin-panel 
                :user="{{ json_encode(session('userData')) }}" 
                :settings="{{ json_encode(session('settings')) }}" 
                :success="{{ json_encode(session('success')) }}"
                :error="{{ json_encode(session('error')) }}">
            </admin-panel>
        @endif
    </div>
</body>
</html>
