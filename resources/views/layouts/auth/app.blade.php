<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ url('media/favicon-company.png') }}">

    <title>FelixCom SMAT - pomiar parametrów środowiskowych</title>

    <!-- CSS -->
    <link rel="stylesheet" href="{{ url('css/animations.css') }}">
    <link rel="stylesheet" href="{{ url('css/main.css') }}">
    <link rel="stylesheet" href="{{ url('css/login.css') }}">
    <link rel="stylesheet" href="{{ url('css/header.css') }}">
    <!-- FONTS -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans:wght@400;700&display=swap" rel="stylesheet">
    <!-- ICONS -->
    <link type="text/css" rel="stylesheet" href="{{ url('css/app.css') }}">
    {{-- VUE --}}
    <script defer src="/js/app.js"></script>
</head>
    
<body>
    <div id="app">
        <main>
            <loginform {{ empty(session('error')) ? '' : 'v-bind:info="'.session('error').'"' }}></loginform>
        </main>
    </div>
</body>
</html>
