<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{{ config('app.name'); }} - Raport danych środowiskowych</title>

</head>
<body>
    {{-- HEADER --}}
    <table style="width:100%;text-align:center;">
        <thead>
            <tr nobr="true">
                <th style="width:30%;">
                    <img src="{{ public_path('media/company-logo.png') }}" alt="{{ config('app.name'); }} logo FelixCom">
                </th>
                <th style="width:70%;font-size:14px;">
                    <br><br>
                    @if (isset($settings['mainHeader']))
                        <span style="font-weight:bold;text-transform:uppercase;">{{ $settings['mainHeader'] }}</span>
                        <br>
                        <span style="font-weight:bold;text-transform:uppercase;">{{ $settings['secondHeader'] }} {{ $settings['companyName'] }}</span>
                        <br>
                        <span style="font-weight:bold;">Przedział czasowy:</span> {{ $startDate }} - {{ $endDate }}
                    @else
                        <span style="font-weight:bold;text-transform:uppercase;">{{ $settings['main_header'] }}</span>
                        <br>
                        <span style="font-weight:bold;text-transform:uppercase;">{{ $settings['second_header'] }} {{ $settings['company_name'] }}</span>
                        <br>
                        <span style="font-weight:bold;">Przedział czasowy:</span> {{ $startDate }} - {{ $endDate }}
                    @endif
                </th>
            </tr>
        </thead>
    </table>
    {{-- INFO BAR --}}
    <table style="padding:4px 0;width:100%;font-size:10px;border-top:1px solid #000;border-bottom:1px solid #000;text-align:center;">
        <thead>
            <tr nobr="true" >
                <th style="width:33%;"><p><span style="font-weight:bold;">Dotyczy:</span> {{ $station['name'] }}</p></th>
                <th style="width:33%;"><p><span style="font-weight:bold;">Data raportu:</span> {{ $raportDate }}</p></th>
                <th style="width:33%;"><p>Raport wygenerowany automatycznie ({{ config('app.name'); }})</p></th>
            </tr>
        </thead>
    </table>
    <br><br>
    {{-- CHARTS BOX --}}
    <table style="width:100%;">
        <tbody>
            @if (isset($statistics['statistics']['charts']) && count($statistics['statistics']['charts']) > 0)
                @foreach($statistics['statistics']['charts'] as $key => $graph) 
                    <tr nobr="true">
                        <td style="padding:12px 0;">
                            @if ($graph == 'temp')
                                <span style="font-size:10px;color:#3a3a3a;">Pomiar temperatury</span>
                            @elseif ($graph == 'press')
                                <span style="font-size:10px;color:#3a3a3a">Pomiar ciśnienia</span>
                            @elseif ($graph == 'humi')
                                <span style="font-size:10px;color:#3a3a3a">Pomiar wilgotności</span>
                            @endif
                            <br>
                            <img style="margin-bottom:10px;" src="{{ public_path('media/graphs/'.$key.'_graph.png') }}">
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>

    {{-- INTERVAL BOX --}}
    <table style="width:100%">
        <tbody>
            <tr nobr="true">
                <th style="width:33%;"></th>
                <th style="width:33%;"></th>
                <th style="width:33%;">
                    <span style="font-size:12px">
                        <span style="font-weight:bold;">Odczyt parametrów co:</span> 1h
                    </span>
                </th>
            </tr>
        </tbody>
    </table>

    {{-- ERRORS TABLE --}}

    {{-- DATA TABLE --}}
    <table style="width:100%;text-align:center;">
        <thead>
            <tr nobr="true">
                <th>
                    <p style="font-size:12px;font-weight:bold;text-transform:uppercase;">WARTOŚCI ŚRODOWISKOWE STACJI: {{ $station['name'] }}</p>
                </th>
            </tr>
        </thead>
    </table>
    <table style="width:100%;text-align:center;" cellpadding="0" cellspacing="0">
        <thead>
            <tr nobr="true">
                @php
                   $headColWidth = round((75 / (count($station['statistics']['sensors']))), 2);
                @endphp
                <td colspan="2" style="font-size:8px;border:1px solid #000;width:25%;">
                    <span>POMIAR</span>
                </td>
                @foreach ($station['statistics']['sensors'] as $sensor)
                    @php
                        if ($sensor['data'][0] !== null) {
                            $sensorColSpan = count((array)$sensor['data'][0]);
                        }   
                    @endphp
                    <td colspan="{{ $sensorColSpan }}" style="font-size:10px;border:1px solid #000;width:{{ $headColWidth }}%;">
                        @if ($sensor['virtualSensorName'] !== null && $sensor['virtualSensorName'] != '') 
                            <span>{{ $sensor['virtualSensorName'] }}</span>
                        @elseif ($sensor['sensorName'] !== null && $sensor['sensorName'] != '') 
                            <span>{{ $sensor['sensorName'] }}</span>
                        @else
                            <span>{{ $sensor['smatSerial'] }}, czujnik nr. {{ $sensor['number'] }}</span>
                        @endif
                        
                        @if ($sensor['virtualSensorLocation'] !== null && $sensor['virtualSensorLocation'] != '') 
                            <span>{{ $sensor['virtualSensorLocation'] }}</span>
                        @elseif ($sensor['sensorLocation'] !== null && $sensor['sensorLocation'] != '') 
                            <span>{{ $sensor['sensorLocation'] }}</span>
                        @endif
                    </td> 
                @endforeach
            </tr>
            <tr nobr="true">
                <td style="font-size:10px;border:1px solid #000;width:12.5%;">
                    <span>Data</span>
                </td>
                <td style="font-size:10px;border:1px solid #000;width:12.5%;">
                    <span>Czas</span>
                </td>
                @foreach ($station['statistics']['sensors'] as $sensor)
                    @if ($sensor['data'][0] !== null)
                        @php
                            if ($sensor['data'][0] !== null) {
                                $valColWidth = round($headColWidth / count((array)$sensor['data'][0]), 2);
                            }   
                        @endphp
                        @foreach ($sensor['data'][0] as $key => $val)
                            <td style="font-size:10px;border:1px solid #000;width:{{ $valColWidth }}%;">
                                @if ($key == 'temp')
                                    <span>°C</span>
                                @elseif ($key == 'press')
                                    <span>hPA</span>
                                @elseif ($key == 'humi')
                                    <span>%</span>
                                @endif
                            </td>
                        @endforeach
                    @endif
                @endforeach
            </tr>
        </thead>
        <tbody>
            @php
                $numberOfSensor = 0;
            @endphp
            @for ($i= 0; $i < count($station['statistics']['datatimes']); ++$i) 
                <tr nobr="true">
                    <td style="font-size:10px;border:1px solid #000;width:12.5%;">
                        <span>{{ date('d/m/Y', strtotime($station['statistics']['datatimes'][$i])) }}</span>
                    </td>
                    <td style="font-size:10px;border:1px solid #000;width:12.5%;">
                        <span>{{ date('H:i', strtotime($station['statistics']['datatimes'][$i])) }}</span>
                    </td>
                    @foreach ($station['statistics']['sensors'][$numberOfSensor]['data'] as $vals)
                        @foreach ($vals as $key => $val)
                            @if (isset($val))
                            @endif
                            <td style="font-size:10px;border:1px solid #000;width:{{ $valColWidth }}%;">
                                @php
                                    if (isset($station['statistics']['sensors'][$numberOfSensor]['data'][$i]->{$key})) {
                                        $value = $station['statistics']['sensors'][$numberOfSensor]['data'][$i]->{$key};
                                    } else {
                                        $value = null;
                                    }
                                @endphp
                                @if ($value !== null)
                                    <span>{{ $value }}</span>
                                @elseif ((int)$value == -9999 || (int)$value == -8888)
                                    <span style="color:red;">BŁĄD</span>
                                @else
                                    <span style="color:red;">OFF</span>
                                @endif
                            </td>
                        @endforeach
                        @if (($numberOfSensor+1) == count($station['statistics']['sensors']))
                            @break;
                        @else
                            @php
                                ++$numberOfSensor;
                            @endphp 
                        @endif
                    @endforeach
                    @php
                        $numberOfSensor = 0;
                    @endphp
                </tr>
            @endfor
        </tbody>
    </table>
</body>
</html>