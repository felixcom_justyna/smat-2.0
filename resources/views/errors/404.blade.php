<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>FelixCom SMAT - pomiar parametrów środowiskowych</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- FONTS -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans:wght@400;700&display=swap" rel="stylesheet">
    <!-- ICONS -->
    <script src="https://kit.fontawesome.com/2ab51abe82.js" crossorigin="anonymous"></script>
    {{-- VUE --}}
    <script defer src="/js/app.js"></script>
</head>
<body>
    <div id="app">
        <div class="error-box">
            <a href="/"><img src="{{ URL::to('/') }}/media/company-logo.png" alt="smat"></a>
            <p>404 | Strony nie znaleziono</p>
        </div>
        <dashboard-footer></dashboard-footer>
    </div>
</body>
</html>
