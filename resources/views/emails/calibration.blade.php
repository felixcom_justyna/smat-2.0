<!DOCTYPE html>
<html>

<head>
    <link href="{{ asset('css/emails/main.css') }}" rel="stylesheet">
</head>

<body>
    <main>
        <h1>Zgłoszono ponowne wzorcowanie stacji pomiarowej w systemie {{ config('app.name'); }}</h1> 
        <p>Obiekt: <strong>{{ $company }}</strong> zgłosił ponowne wzorcowanie stacji pomiarowej, numer seryjny: <strong>{{ $serial }}</strong></p>
        <br>
        <hr>
        <img src="{{ asset('media/company-logo.png') }}" alt="smat felixcom">
    </main>
</body>

</html>

