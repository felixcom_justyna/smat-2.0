<!DOCTYPE html>

<html>

<head>
    <link href="{{ asset('css/emails/main.css') }}" rel="stylesheet">
</head>

<body>
    <main>
        <h1>Dostęp do systemu {{ config('app.name'); }}</h1>
        <p>
            Na ten adres e-mail utworzono nowe konto do systemu {{ config('app.name'); }}.
            Poniżej znajdziesz nowe, losowo wygenerowane hasło do konta.
            Po zalogowaniu, w ramach bezpieczeństwa, zmień hasło na własne.
        </p>
        <br>
        <p>
            Hasło do konta: <span>{{ $password }}</span>
        </p>
        <hr>
        <p class="base-info">
            Ta wiadomość została wygenerowana automatycznie przez system {{ config('app.name'); }} - <span>prosimy na nią nie odpowiadać</span>.
            Wiadomość Cię nie dotyczy? Powiadom nas o tym: <a href="mailto:biuro@felixcom.pl">biuro@felixcom.pl</a>
        </p>
        <img src="{{ asset('media/company-logo.png') }}" alt="smat felixcom">
    </main>
</body>

</html>

