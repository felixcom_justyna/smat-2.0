<!DOCTYPE html>
<html>

<head>
    <link href="{{ asset('css/emails/main.css') }}" rel="stylesheet">
</head>

<body>
    <main>
        <h1>Autoraport systemu {{ config('app.name'); }}</h1>
        <p>
            W załączniku znajdziesz wygenerowane automatycznie dokumenty zawierające dane środowiskowe systemu {{ config('app.name'); }}
            z wybranego przedziału czasowego.
        </p>
        <hr>
        <img src="{{ asset('media/company-logo.png') }}" alt="smat felixcom">
    </main>
</body>

</html>

