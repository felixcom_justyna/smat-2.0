<!DOCTYPE html>
<html>

<head>
    <link href="{{ asset('css/emails/main.css') }}" rel="stylesheet">
</head>

<body>
    <main>
        <h1>Odnotowano offline w systemie {{ config('app.name'); }}, dla obiektu: {{ $company->company_name }} </h1> 
        <p>Poniżej znajdziesz listę stacji które nie wykazują aktywności od ponad 10min:</p>
        <table class="darkTable">
            <thead>
                <tr>
                    <th>Numer seryjny stacji</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($serials as $key => $serial)
                <tr>
                    <td>{{ $serial }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <br>
        <hr>
        <img src="{{ asset('media/company-logo.png') }}" alt="smat felixcom">
    </main>
</body>

</html>

