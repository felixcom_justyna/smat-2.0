<!DOCTYPE html>
<html>

<head>
    <link href="{{ asset('css/emails/main.css') }}" rel="stylesheet">
</head>

<body>
    <main>
        <h1>Przekroczono progi globalne w systemie {{ config('app.name'); }}, dla obiektu: {{ $company->company_name }} </h1> 
        <p>Poniżej znajdziesz listę odnotowanych wartości:</p>
        <table class="darkTable">
            <thead>
                <tr>
                    <th>Dnia</th>
                    <th>Numer seryjny stacji</th>
                    <th>Czujnik nr.</th>
                    <th>Wartość</th>
                    <th>Odnotowano</th>
                    <th>Próg min</th>
                    <th>Próg max</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($alerts as $alert)
                <tr>
                    <td>{{  date('d/m/Y H:i', strtotime($alert->date))  }}</td>
                    <td>{{  $alert->serial_number  }}</td>
                    <td>{{  $alert->number  }}</td>
                    @if ($alert->value_type == 'temp')
                        <td>Temperatura</td>
                        <td>{{ $alert->measured_value }} °C</td>
                    @elseif ($alert->value_type == 'humi')
                        <td>Wilgotność</td>
                        <td>{{ $alert->measured_value }} %</td>
                    @else
                        <td>Ciśnienie</td>
                        <td>{{ $alert->measured_value }} hPA</td>
                    @endif
                    @if ($vals[$alert->value_type]['min'] === NULL)
                        <td>-</td>
                    @else
                        <td>{{ $vals[$alert->value_type]['min'] }}</td>
                    @endif
                    @if ($vals[$alert->value_type]['max'] === NULL)
                        <td>-</td>
                    @else
                        <td>{{ $vals[$alert->value_type]['max'] }}</td>
                    @endif

                </tr>
                @endforeach
            </tbody>
        </table>
        <br>
        <hr>
        <img src="{{ asset('media/company-logo.png') }}" alt="smat felixcom">
    </main>
</body>

</html>

