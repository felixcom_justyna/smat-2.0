<!DOCTYPE html>

<html>

<head>
    <link href="{{ asset('css/emails/main.css') }}" rel="stylesheet">
</head>

<body>
    <main>
        <h1>Wiadomość testowa</h1>
        <p>
            Wysyłka wiadomości e-mail systemu {{ config('app.name'); }} działa <span>poprawnie</span>.
        </p>
        <br>
        <hr>
        <p class="base-info">
            Ta wiadomość została wygenerowana automatycznie przez system {{ config('app.name'); }} - <span>prosimy na nią nie odpowiadać</span>.
            Wiadomość Cię nie dotyczy? Powiadom nas o tym: <a href="mailto:biuro@felixcom.pl">biuro@felixcom.pl</a>
        </p>
        <img src="{{ asset('media/company-logo.png') }}" alt="smat felixcom">
    </main>
</body>

</html>

