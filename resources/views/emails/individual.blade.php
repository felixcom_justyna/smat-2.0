<!DOCTYPE html>
<html>

<head>
    <link href="{{ asset('css/emails/main.css') }}" rel="stylesheet">
</head>

<body>
    <main>
        <h1>Przekroczono progi szczególne w systemie {{ config('app.name'); }}, dla obiektu: {{ $company->company_name }} </h1> 
        <p>Poniżej znajdziesz listę odnotowanych wartości:</p>
        <table class="darkTable">
            <thead>
                <tr>
                    <th>Nazwa stacji</th>
                    <th>Nazwa czujnika</th>
                    <th>Lokalizacja czujnika</th>
                    <th>Czujnik nr.</th>
                    <th>Dnia</th>
                    <th>Wartość</th>
                    <th>Odnotowano</th>
                    <th>Próg min</th>
                    <th>Próg max</th>
                </tr>
            </thead>
            <tbody>
                {{-- @if ($flag == 'toAll') --}}
                    @foreach ($stations as $station)
                        @foreach ($station as $sensor) 
                            @foreach($alerts as $alert)
                                @if ($alert->serial_number == $sensor->serialNumber &&
                                $alert->number == $sensor->sensorNumber &&
                                (isset($sensor->{ $alert->value_type.'_min' }) || isset($sensor->{ $alert->value_type.'_max' })))

                                {{-- @if (isset($sensor->{ $alert->value_type.'_min' }) || isset($sensor->{ $alert->value_type.'_max' })) --}}

                                <tr>
                                    <td>{{ $sensor->stationName }}</td>
                                    @if ($sensor->sensorName !== NULL)
                                        <td>{{ $sensor->sensorName }}</td>
                                    @else
                                        <td>-</td>
                                    @endif
                                    @if ($sensor->sensorLocation !== NULL)
                                        <td>{{ $sensor->sensorLocation }}</td>
                                    @else
                                        <td>-</td>
                                    @endif
                                    <td>{{ $alert->number }}</td>
                                    <td>{{ date('d/m/Y H:i', strtotime($alert->date)) }}</td>
                                    @if ($alert->value_type == 'temp')
                                        <td>Temperatura</td>
                                        <td>{{ $alert->measured_value }} °C</td>
                                    @elseif ($alert->value_type == 'humi')
                                        <td>Wilgotność</td>
                                        <td>{{ $alert->measured_value }} %</td>
                                    @else
                                        <td>Ciśnienie</td>
                                        <td>{{ $alert->measured_value }} hPA</td>
                                    @endif
                                    <td>{{ $sensor->{ $alert->value_type.'_min' } }}</td>
                                    <td>{{ $sensor->{ $alert->value_type.'_max' } }}</td>
                                </tr>


                                @endif
                            @endforeach
                            
                            @endforeach
                    @endforeach
                {{-- @elseif ($flag == 'toUser') --}}
                    {{-- @foreach ($stations as $stationId => $data)
                        <tr>
                            <td>{{ date('d/m/Y H:i', strtotime($alerts[$i]->date)) }}</td>
                            <td>{{ $sensor->stationName }}</td>
                            @if ($sensor->sensorName !== NULL)
                                <td>{{ $sensor->sensorName }}</td>
                            @else
                                <td>-</td>
                            @endif
                            @if ($sensor->sensorLocation !== NULL)
                                <td>{{ $sensor->sensorLocation }}</td>
                            @else
                                <td>-</td>
                            @endif
                            <td>{{ $sensor->sensorNumber }}</td>
                            @if ($alerts[$i]->value_type == 'temp')
                                <td>Temperatura</td>
                                <td>{{ $alerts[$i]->measured_value }} °C</td>
                            @elseif ($alerts[$i]->value_type == 'humi')
                                <td>Wilgotność</td>
                                <td>{{ $alerts[$i]->measured_value }} %</td>
                            @else
                                <td>Ciśnienie</td>
                                <td>{{ $alerts[$i]->measured_value }} hPA</td>
                            @endif
                            @if (isset($sensor->{ $alerts[$i]->value_type.'_min' }))
                            <td>{{ $sensor->{ $alerts[$i]->value_type.'_min' } }}</td>
                            <td>{{ $sensor->{ $alerts[$i]->value_type.'_max' } }}</td>
                            @endif
                        </tr>
                        <?php //$i +=1; ?>
                    @endforeach --}}
                {{-- @endif --}}
            </tbody>
        </table>
        <br>
        <hr>
        <img src="{{ asset('media/company-logo.png') }}" alt="smat felixcom">
    </main>
</body>

</html>

