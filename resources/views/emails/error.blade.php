<!DOCTYPE html>
<html>

<head>
    <link href="{{ asset('css/emails/main.css') }}" rel="stylesheet">
</head>

<body>
    <main>
        <h1>Odnotowano błąd w systemie {{ config('app.name'); }}, dla obiektu: {{ $company->company_name }} </h1> 
        <p>Poniżej znajdziesz listę odnotowanych błędów:</p>
        <table class="darkTable">
            <thead>
                <tr>
                    <th>Dnia</th>
                    <th>Numer seryjny stacji</th>
                    <th>Czujnik nr.</th>
                    <th>Wartość</th>
                    <th>Numer błędu</th>
                    <th>Treść błędu</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($errors as $error)
                <tr>
                    <td>{{  date('d/m/Y H:i', strtotime($error->date))  }}</td>
                    <td>{{  $error->serial_number  }}</td>
                    <td>{{  $error->number  }}</td>
                    @if ($error->value_type == 'temp')
                        <td>Temperatura</td>
                    @elseif ($error->value_type == 'humi')
                        <td>Wilgotność</td>
                    @else
                        <td>Ciśnienie</td>
                    @endif
                    <td>{{ $error->error_type }}</td>
                    @if ((int)$error->error_type == -9999)
                        <td>Błąd czujnika</td>
                    @else
                    <td>Błąd pomiaru</td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
        <br>
        <hr>
        <img src="{{ asset('media/company-logo.png') }}" alt="smat felixcom">
    </main>
</body>

</html>

