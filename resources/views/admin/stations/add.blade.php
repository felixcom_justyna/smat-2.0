@extends('layouts.admin.app')

@section('content')
<h2 class="title decor-line main-font">DODAJ STANOWISKO POMIAROWE</h2>
<p class="textbox text-font">
    W poniższej sekcji możesz utworzyć stanowisko pomiarowe wybierając 
    dostępne czujniki aktualnie skonfigurowanych skrzyni pomiarowych SMAT.
</p>
<section class="wrapper">
    <form method="POST" action="{{ route('admin.stations.add') }}" class="settings">
        @csrf
        <div>
            <label class="main-font">Nazwa stanowiska:</label>
            <input type="text" name="name" placeholder="podaj własną nazwę stanowiska">
        </div>
        <div>
            <label class="main-font">Lokalizacja:</label>
            <input type="text" name="location" placeholder="podaj własną nazwę lokalizacji" value="">
        </div>
        <div>
            <div>
                <label class="main-font">Widoczność stanowiska:</label>
                <div class="onoffswitch">
                    <input type="checkbox" name="onoffswitch" id="station-status" class="onoffswitch-checkbox" tabindex="0">
                    <label class="onoffswitch-label" for="station-status">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                </div>
            </div>
        </div>
        <div>
            <div>
                <label class="main-font">Wysyłka e-mail o przekroczonych progach:</label>
                <div class="onoffswitch">
                    <input type="checkbox" name="onoffswitch" id="station-alerts-status" class="onoffswitch-checkbox" tabindex="0">
                    <label class="onoffswitch-label" for="station-alerts-status">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                </div>
            </div>
        </div>
        <input class="btn default-btn m-btn" type="submit" value="Stwórz stanowisko">
    </form>
</section>
@endsection