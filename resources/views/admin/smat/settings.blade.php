@extends('layouts.admin.app')

{{-- @section('content')
<h2 class="title decor-line main-font">USTAWIENIA STACJI</h2>
<p class="textbox text-font">
    W poniższej sekcji możesz edytować dane skrzyni pomiarowej SMAT oraz jej czujników.
    Numer seryjny jest nadawny jednorazowo i nie można go zmienić
</p>
<section class="wrapper">
    <div class="stations-list">
        @for ($i = 0; $i < count($data['smatBoxes']); ++$i)
            @if ($i == 0)
               {!! '<div class="station checked">' !!}
            @else
                {!! '<div class="station">' !!}
            @endif
            {!! '<h3 class="text-font text-up">'.$data['smatBoxes'][$i]->serial_number.'</h3>' !!}
            {!! '</div>' !!}
        @endfor
    </div>
    <form method="POST" action="{{ route('admin.smat.edit', $data['smatBoxes'][0]->id) }}" class="settings">
        @csrf
        <div>
            <label class="main-font">Numer seryjny:</label>
            <input type="text" name="serialNumber" readonly="readonly" value="{{ $data['smatBoxes'][0]->serial_number }}">
        </div>
        <div>
            <label class="main-font">Lokalizacja stacji:</label>
            <input type="text" name="location" placeholder="lokalizacja montażu" value="{{ $data['smatBoxes'][0]->location }}">
        </div>
        <div>
            <div>
                <label class="main-font">Data wzorcowania:</label>
                <input type="date" name="calibrationDate" value="{{ $data['smatBoxes'][0]->calibration_date }}">
            </div>
        </div>
        <div>
            <label class="main-font">Ważność wzorcowania:</label>
            <select name="calibrationDays">
                <option value="365" 
                @if ($data['smatBoxes'][0]->calibration_days == 365)
                    {{ 'selected' }}
                @endif
                >1 rok</option>
                <option value="730"
                @if ($data['smatBoxes'][0]->calibration_days == 730)
                    {{ 'selected' }}
                @endif
                >2 lata</option>
                <option value="1825"
                @if ($data['smatBoxes'][0]->calibration_days == 1825)
                    {{ 'selected' }}
                @endif
                >5 lat</option>
            </select>
        </div>
        <div>
            <label class="main-font">Kalibracja +/- temperatury:</label>
            <input type="number" step="0.1" value="{{ $data['smatBoxes'][0]->temp_diff }}" lang="pl" name="diffTemp">
        </div>
        <div>
            <label class="main-font">Kalibracja +/- ciśnienia:</label>
            <input type="number" step="0.1" value="{{ $data['smatBoxes'][0]->press_diff }}" lang="pl" name="diffPress">
        </div>
        <div>
            <label class="main-font">Kalibracja +/- wilgotności:</label>
            <input type="number" step="0.1" value="{{ $data['smatBoxes'][0]->humi_diff }}" lang="pl" name="diffHumi">
        </div>
        <input class="btn default-btn m-btn" type="submit" value="Edytuj stację">
    </form>
</section>
@endsection --}}