/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('loginform', require('./components/LoginForm.vue').default);

Vue.component('dashboard-head', require('./components/DashboardHead.vue').default);
Vue.component('dashboard-panel', require('./components/DashboardPanel.vue').default);
Vue.component('dashboard-content', require('./components/DashboardContent.vue').default);
Vue.component('dashboard-footer', require('./components/DashboardFooter.vue').default);

Vue.component('admin-head', require('./components/AdminHead.vue').default);
Vue.component('admin-nav', require('./components/AdminNav.vue').default);
Vue.component('admin-panel', require('./components/AdminPanel.vue').default);
Vue.component('admin-content', require('./components/AdminContent.vue').default);

// add DateJs library
import datejs from 'datejs';
Object.defineProperty(Vue.prototype, '$datejs', { value: datejs });

// add vue session
import VueSession from "vue-session";
Vue.config.productionTip = false
Vue.use(VueSession)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const colors = {
    methods: {
        byte2Hex(n) {
            var nybHexString = "0123456789ABCDEF";
            return String(nybHexString.substr((n >> 4) & 0x0F,1)) + nybHexString.substr(n & 0x0F,1);
        },
        rgbToHex(r,g,b) {
            return '#' + this.byte2Hex(r) + this.byte2Hex(g) + this.byte2Hex(b);
        }
    }
}
const requests = {
    methods: {
        sendDeleteReq(id, path) {
            console.log(id)
        }
    }
}

const app = new Vue({
    el: '#app',
    config: {
        token: document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
        name: 'SMAT'
    },
    mixins: [colors, requests],
});
